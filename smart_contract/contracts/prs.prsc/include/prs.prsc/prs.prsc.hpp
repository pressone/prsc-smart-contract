/**
 *  @file
 *  @copyright defined in LICENSE.txt
 */

#pragma once

#include <eosio/eosio.hpp>
#include <eosio/asset.hpp>
#include <eosio/singleton.hpp>

#include <string>
#include <map>

using eosio::name;
using eosio::asset;
using eosio::datastream;

namespace prs {

    static bool DEBUG = true;

	enum RESULT_CODE {
		OK = 			1, 
		WARNING = 		2, 
		ERROR = 		3,
		FATAL_ERROR =	4 
	};

    static const char * ID           = "id";
    static const char * USER_ADDRESS = "user_address";
    static const char * TYPE         = "type";
    static const char * META         = "meta";
    static const char * DATA         = "data";
    static const char * HASH         = "hash";
    static const char * SIGANTURE    = "signature";    

    static constexpr uint8_t default_discount     = 100;
    static constexpr double  default_spr_per_byte = 0.1;

    static constexpr eosio::name prs_saving{"prs.saving"_n};
    static constexpr eosio::name prs_pool{"prs.pool"_n};

    CONTRACT prsc : public eosio::contract {
       
        public: 
            using contract::contract;

            /// contract constructor
            prsc(name s, name code, datastream<const char*> ds);

            //distructor
            ~prsc();

            // 3.1 Publisher Manager                
            // parameters: 
            //  ** publisher_manager : provide authorize (should be an EOS account?)
            //  ** operation : allow/deny/pubAddress
            //  ** data      : pubAddress
            //  ** topic     : topic_id
            ACTION save(name        caller,
                        std::string id, 
                        std::string user_address,
                        std::string type,
                        std::string meta,
                        std::string data,
                        std::string hash,
                        std::string signature);

            // Set price per byte (in SRP)
            // parameters:
            // ** spr_per_byte : SPR per byte 
            ACTION setpricpbt(double spr_per_byte, uint8_t discount);

            // Set unit price (byte/spr) and discount for specified account
            // if existed in this table, total amount to pay calculate as below:
            // total_amount_to_pay = content_in_bytes * price_per_byte * ( discount / 100 )
            // parameters:
            // ** payer : account name
            // ** spr_per_byte : spr per byte, when exist,this value will overcome global setting
            // ** discount : discount for this account, should between [0,100]
            ACTION setdisct(name payer, double spr_per_byte, uint8_t discount);

            // Remove special discount for an account
            // parameters:
            // ** payer : payer to be removed

            ACTION deldist(name payer);

            using save_action        = eosio::action_wrapper<"save"_n, &prsc::save>;
            using set_pricpbt_action = eosio::action_wrapper<"setpricpbt"_n, &prsc::setpricpbt>;
            using set_disct_action   = eosio::action_wrapper<"setdisct"_n, &prsc::setdisct>;
            using del_dist_action    = eosio::action_wrapper<"deldist"_n, &prsc::deldist>;
                        
        private:

            struct [[eosio::table]] api_payment_discount_info {
                name    account_name;
                double  spr_per_byte = 0.1;
                uint8_t discount = 100;    //no discount
                uint64_t primary_key() const {return account_name.value;}
            };

            struct [[eosio::table("global")]] prs_global_state { 
                prs_global_state() { }    
                
                double  spr_per_byte;
                uint8_t default_discount;

                EOSLIB_SERIALIZE(prs_global_state, (spr_per_byte)(default_discount))
            };

            using discount_table =  eosio::multi_index<"discount"_n, api_payment_discount_info>;            
            using global_state_singleton =  eosio::singleton< "global"_n, prs_global_state>;

            discount_table         _discount; 
            global_state_singleton _global;
            prs_global_state       _gstate;

            static prs_global_state get_default_parameters();            

            RESULT_CODE do_send_request_auth(name         sender,
                                             std::string& auth_content);

            RESULT_CODE do_charge_caller(name         payer,
                                         eosio::asset amount);
                                             
            void printlog(std::string& str);
    };
}