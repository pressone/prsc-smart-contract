
add_executable( prs.prsc ${CMAKE_CURRENT_SOURCE_DIR}/src/prs.prsc.cpp )
target_compile_options( prs.prsc PUBLIC -abigen )
get_target_property(BINOUTPUT prs.prsc BINARY_DIR)
target_compile_options( prs.prsc PUBLIC -abigen_output=${BINOUTPUT}/${TARGET}.abi )
target_compile_options( prs.prsc PUBLIC -contract prsc )

target_include_directories(prs.prsc
   PUBLIC
   ${CMAKE_CURRENT_SOURCE_DIR}/include
   ${CMAKE_CURRENT_SOURCE_DIR}/../common)

set_target_properties(prs.prsc
   PROPERTIES
   RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
