#include <prs.prsc/prs.prsc.hpp>
#include <prs.prsc/json.hpp>

namespace prs {

    using namespace eosio;
    using std::string;
    using json = nlohmann::json;

    prsc::prsc(name s, name code, datastream<const char*> ds) 
    :contract(s, code, ds),
    _discount(s, s.value),
    _global(s, s.value) 
    {
        _gstate = _global.exists() ? _global.get() : get_default_parameters();
        _global.set(_gstate,get_self());
    }

    prsc::prs_global_state prsc::get_default_parameters() {        
        prs_global_state dp;
        dp.spr_per_byte = default_spr_per_byte;
        dp.default_discount = default_discount;
        return dp;
    }

    prsc::~prsc() {
        _global.set(_gstate, get_self());
    }

    ACTION prsc::setpricpbt(double spr_per_byte, uint8_t default_discount) {
        require_auth(get_self());
        check(spr_per_byte >= 0 , "spr_per_byte can't be negative");
        check(default_discount >= 0 && default_discount <= 100, "default_discount should between [0, 100]");
        _gstate.spr_per_byte = spr_per_byte;
        _gstate.default_discount = default_discount;
        _global.set(_gstate, get_self());
    }

    ACTION prsc::setdisct(name payer, double spr_per_byte, uint8_t discount) {
        require_auth(get_self());
        check(is_account(payer), "invalid payer");
        check(spr_per_byte >=0, "price should large than 0");
        check(discount >= 0 && discount <= 100, "discount should between [0, 100]");

        auto payer_iter = _discount.find(payer.value);
        if (payer_iter == _discount.end()) {
            _discount.emplace(_self, [&](auto& item) {
                item.account_name = payer;
                item.spr_per_byte = spr_per_byte;
                item.discount = discount;
            });             

        } else {
            _discount.modify(payer_iter, _self, [&](auto& item) {
                item.spr_per_byte = spr_per_byte;
                item.discount = discount;
            });              
        }        
    }

    ACTION prsc::deldist(name payer) {
        require_auth(get_self());
        check(is_account(payer), "invalid payer");

        auto& payer_iter = _discount.get(payer.value, "payer not exist");
        _discount.erase(payer_iter);
    }

    ACTION prsc::save(
                name   caller,
                string id, 
                string user_address,
                string type,
                string meta,
                string data,
                string hash,
                string signature) {
        require_auth(caller);
        require_recipient(caller);
        
        string json_content = "";

        json j;
        j[ID]           = id;
        j[USER_ADDRESS] = user_address;
        j[TYPE]         = type;
        j[META]         = meta;
        j[DATA]         = data;
        j[HASH]         = hash;
        j[SIGANTURE]    = signature;

        json_content = j.dump();

        double  amount;
        uint8_t discount;
        double  spb;
        
        //get caller price
        auto payer_iter = _discount.find(caller.value);
        if (payer_iter == _discount.end()) {
            // Can not find payer, user default discount;
            discount = _gstate.default_discount;
            spb = _gstate.spr_per_byte;
        } else {
            //user specified rate and discount;
            discount = payer_iter->discount;
            spb = payer_iter->spr_per_byte;
        }

        double discount_rate = discount / 100.0;
        amount = data.size() * spb * discount_rate;

        int num = (int)(amount * pow(10, 4));
        asset payout = asset(num, symbol("SRP", 4));    

        //check (false, payout);
        if (payout.amount > 0) {
            check (OK == do_charge_caller(caller, payout), "collect payment failed, please verify you have enough SPR in your liquid account");
        }            

        check (OK == do_send_request_auth(caller, json_content), "do_send_request_auth failed");
    }

    RESULT_CODE prsc::do_send_request_auth(name    sender,
                                           string& auth_content) {
		action(
			permission_level{_self, "active"_n},
			"prsc.oracle"_n,
			"addauthreq"_n,
			std::make_tuple(sender, auth_content)
		).send();
    
        return OK;
    }

    RESULT_CODE prsc::do_charge_caller(name  payer,
                                       asset amount) {
        string memo = "";
        action(
            permission_level{ payer, "active"_n },
            "eosio.token"_n,
            "transfer"_n,
            std::make_tuple(payer, prs_saving, amount, memo)
        ).send();                                           

        return OK;                                                
    }

    void  prsc::printlog(string& str) {
        if (DEBUG) {
           print (str, "||" );
        }
    }

} /// namespace prs