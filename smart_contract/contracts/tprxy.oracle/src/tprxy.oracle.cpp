#include <tprxy.oracle/tprxy.oracle.hpp>
#include <eosio/system.hpp>
#include <eosio/transaction.hpp>
#include <eosio/crypto.hpp>

namespace prs{

	using std::string;
	using namespace eosio;

	ACTION toracle::addauthreq(name     sender, 
	 						   uint64_t req_id, 
							   string   mixin_trace_id,
                               string   auth_content, 
							   string   memo) {
		print("toracle::addauthreq called");
		
		require_auth("prs.tproxy"_n);

 		auth_index auth(get_self(), get_first_receiver().value);
		auth.emplace(_self, [&](auto& obj) {
				obj.id = req_id;
				obj.mixin_trace_id = mixin_trace_id;
				obj.auth_content = auth_content;
				obj.timestamp_req = get_current_block_time();
				obj.trx_id = get_trx_id();
		});
	}

	ACTION toracle::cnfmauth(uint64_t    req_id,
					         std::string memo) {
		print("toracle::cnfmauth called");
		require_auth("prs.tproxy"_n);

 		auth_index auth(get_self(), get_first_receiver().value);	
		auto iter = auth.find(req_id);
		if (iter == auth.end()) {
			check(false, "cnfmauth failed, invalid req_id");
		} else {
			auth.erase(iter);
		}		
	}

	ACTION toracle::adddeposit(name     sender,
					           uint64_t req_id,
							   string   mixin_trace_id,
					           name     user, 
					           asset    amount,
					           string   memo) {
		print("toracle::addtrans called");
		require_auth("prs.tproxy"_n);

		deposit_index deposit(get_self(), get_first_receiver().value);
		deposit.emplace(_self, [&](auto& obj) {
				obj.id = req_id;
				obj.mixin_trace_id = mixin_trace_id,
				obj.user = user;
				obj.amount = amount;
				obj.timestamp_received = get_current_block_time();
				obj.trx_id_received = get_trx_id();
				obj.status = RECEIVED;
		});
	}

	ACTION toracle::cnfmdeposit(uint64_t req_id,
						        string   memo) {
		print("toracle::cnfmauth called");
		require_auth("prs.tproxy"_n);

		deposit_index deposit(get_self(), get_first_receiver().value);
		auto iter = deposit.find(req_id);

		if (iter == deposit.end()) {
			check(false, "cnfmdeposit failed, invalid req_id");
		} else {
			deposit.erase(iter);
		}
	}	

	ACTION toracle::addpayment(name     sender,
			                   name     bp,
					           asset    bpayamount,
					           asset    vpayamount) {
        print("oracle::addpaymetn called");
		require_auth("eosio"_n);

		payment_index payment(get_self(), get_first_receiver().value);
		payment.emplace(_self, [&](auto& obj) {
			obj.id = payment.available_primary_key();
			obj.bp_name = bp;
			obj.bpay_amount = bpayamount;
			obj.vpay_amount = vpayamount;
			obj.trx_id = get_trx_id();
		});
	}

	ACTION toracle::cnfmpaymt (name caller,
	                           uint64_t id,
					           string memo) {
        print("oracle::cnfmpaymt called");

	    require_auth(_self);

 		payment_index payment(get_self(), get_first_receiver().value);	
		auto iter = payment.find(id);
		if (iter == payment.end()) {
			check(false, "invalid payment id");
		} else {
			payment.erase(iter);			
		}
	}
	
	ACTION toracle::updresult(uint64_t req_id,
		                 	  uint8_t  type,
						      string   auth_data,
							  bool     auth_result,							 
							  string   memo) {
		print("oracle::updresult called");

		require_auth(_self);

		//if deposit
		if (type == DEPOSIT) {
		
			//get deposit
			deposit_index deposit(get_self(), get_first_receiver().value);
			auto iter = deposit.find(req_id);			
			check(iter != deposit.end(), "Can not find deposit recorder");

			deposit.modify(iter, _self, [&](auto &upd){
				check(upd.status == RECEIVED, "invalid deposit status");
				if (auth_result) {
					// auth successful, user paid certain amount of PRS to mixin
					// send certain amount of SRP from tprxy.oracle to user
					action(
						permission_level{"tprxy.oracle"_n, "active"_n},
						"eosio.token"_n,
						"transfer"_n,
						std::make_tuple(_self,
							upd.user,
							asset(upd.amount.amount, upd.amount.symbol),
							std::string(""))
						).send();
					upd.status = PAID;
				} else {
					// auth failed, user didn't pay certain amount of PRS to mixin
					// send certain amount token from tprxy.oracle back to prs.tproxy
					action(
						permission_level{"tprxy.oracle"_n, "active"_n},
						"eosio.token"_n,
						"transfer"_n,
						std::make_tuple(_self,
							"prs.tproxy"_n,
							asset(upd.amount.amount, upd.amount.symbol),
							std::string(""))
						).send();
				}
			});

			//send deposit auth result to prs.tproxy
			action(
				permission_level{get_self(), "active"_n}, 
				"prs.tproxy"_n,
				"updauth"_n,
				std::make_tuple(get_self(),
								req_id, 
								auth_result,
								auth_data,
								string("deposit auth result from tprxy.oracle"))
			).send();	

			//remove deposit req
			deposit.erase(iter);
		} else {
			check(false, "unsupported type");
		}	

		//remove auth request
		auth_index auth(get_self(), get_first_receiver().value);	
		auto iter = auth.find(req_id);
		if (iter == auth.end()) {
			check(false, "invalid req_id");
		} else {
			auth.erase(iter);			
		}		
	}

	ACTION toracle::purgeauth(string memo) {
		print("oracle::purgeauth called");
		require_auth(_self);

		// purge all auth contents
	 	auth_index auth(get_self(), get_first_receiver().value);        
		for(auto itr = auth.begin(); itr != auth.end();) {
            itr = auth.erase(itr);        
		}	 	
	}

	ACTION toracle::purgedep(string memo) {
		print("oracle::purgedep called");
		require_auth(_self);

		// purge all deposit contents
	 	deposit_index deposit(get_self(), get_first_receiver().value);        
		for(auto itr = deposit.begin(); itr != deposit.end();) {
            itr = deposit.erase(itr);        
		}	 	
	}

	ACTION toracle::purgepaym(std::string memo) {
	    print("oracle::purgepaym called");						 
	    require_auth(_self);	
	 	   
 		payment_index payment(get_self(), get_first_receiver().value);		     
		for(auto itr = payment.begin(); itr != payment.end();) {
            itr = payment.erase(itr);        
		}			
	}

	checksum256 toracle::get_trx_id() {
    	size_t size = transaction_size();
    	char buf[size];
    	size_t read = read_transaction( buf, size );
    	check( size == read, "read_transaction failed");
    	return sha256( buf, read );
	}

	uint32_t toracle::get_current_block_time() {
		eosio::time_point now = eosio::current_block_time(); 
        uint32_t time_stamp = now.sec_since_epoch();
		return time_stamp;
	}	
} //namespace
