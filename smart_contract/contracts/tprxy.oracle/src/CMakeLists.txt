cmake_minimum_required(VERSION 3.5)
project(oracle_project)

set(EOSIO_WASM_OLD_BEHAVIOR "Off")
find_package(eosio.cdt)

add_contract(toracle tprxy.oracle tprxy.oracle.cpp)
target_include_directories(tprxy.oracle PUBLIC ${CMAKE_SOURCE_DIR}/../include)