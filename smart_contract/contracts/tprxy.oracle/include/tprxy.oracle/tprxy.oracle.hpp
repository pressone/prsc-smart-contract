#pragma once

#include <eosio/eosio.hpp>
#include <eosio/asset.hpp>
#include <string>

using std::string;
using eosio::name;
using eosio::datastream;
using eosio::asset;

namespace prs{

	using namespace eosio;

	enum TASK_TYPE {
		DEPOSIT  = 1, 
		WITHDRAW = 2
	};

	enum DEPOSIT_STATUS {
		RECEIVED = 1, 
		PAID     = 2,
		RETURNED = 3
	};

	class [[eosio::contract]] toracle : public contract {
		
	  public:
		toracle( name receiver, name code, datastream<const char*> ds )
			   :contract(receiver, code, ds) {}

		//Actions		
		ACTION addauthreq(name        sender,
						  uint64_t    req_id,      
						  std::string mixin_trace_id,      
                          std::string auth_content,
						  std::string memo);

		ACTION cnfmauth(uint64_t    req_id,
					    std::string memo);

		ACTION adddeposit(name        sender,
						  uint64_t    req_id, 
						  std::string mixin_trace_id,
                          name        user, 
						  asset       amount,
						  std::string memo);

		ACTION cnfmdeposit(uint64_t req_id,
						   std::string memo);

		ACTION addpayment(name     sender,
			              name     bp,
						  asset    bpayamount,
						  asset    vpayamount);

		ACTION cnfmpaymt(name caller,
						 uint64_t id,
						 std::string memo);

	    ACTION updresult(uint64_t    req_id,
		                 uint8_t     type,
						 std::string auth_data,
						 bool        auth_result,
						 std::string memo);						 

		//test only
		ACTION purgeauth(std::string memo);		
		ACTION purgedep(std::string memo);
		ACTION purgepaym(std::string memo);

		//Action Wrappers
		using addauthreq_action  = action_wrapper<"addauthreq"_n,  &toracle::addauthreq>;
		using cnfmauth_action    = action_wrapper<"cnfmauth"_n,    &toracle::cnfmauth>;
		using adddeposit_action  = action_wrapper<"adddeposit"_n,  &toracle::adddeposit>;
		using cnfmdeposit_action = action_wrapper<"cnfmdeposit"_n, &toracle::cnfmdeposit>;		
		using addpayment_action  = action_wrapper<"addpayment"_n,  &toracle::addpayment>;
		using cnfmpaymt_action   = action_wrapper<"cnfmpaymt"_n,   &toracle::cnfmpaymt>;
		using updresult_action   = action_wrapper<"updresult"_n,   &toracle::updresult>;		
		using purgeauth_action   = action_wrapper<"purgeauth"_n,   &toracle::purgeauth>;
		using purgeadep_action   = action_wrapper<"purgedep"_n,    &toracle::purgedep>;
		using purgepaym_action   = action_wrapper<"purgepaym"_n,   &toracle::purgepaym>;		

	  private:

        struct  [[eosio::table]]  authitems {
            uint64_t          id = 0;
			std::string       mixin_trace_id = "";
			std::string       auth_content = "";			
			uint32_t          timestamp_req = 0;
			checksum256       trx_id;

            uint64_t primary_key() const { return id; }
        };

		struct [[eosio::table]] deposititems {
			uint64_t    id = 0;
			std::string mixin_trace_id = "";
			name        user;
			asset       amount;			
			uint32_t    timestamp_received = 0;			
			uint32_t    timestamp_processed = 0;
			checksum256 trx_id_received;
			checksum256 trx_id_processed;
			uint8_t     status;

            uint64_t primary_key() const { return id; }			
		};

		struct [[eosio::table]] paymentitems {
			uint64_t id = 0;
			name     bp_name;
			asset    bpay_amount;
			asset    vpay_amount;
			checksum256 trx_id;

			uint64_t primary_key() const { return id; }
		};

        typedef multi_index<"auth"_n,    authitems>    auth_index;
		typedef multi_index<"deposit"_n, deposititems> deposit_index;
		typedef multi_index<"payment"_n, paymentitems> payment_index;

		checksum256 get_trx_id();

		uint32_t get_current_block_time();

	}; //class
}