#pragma once

#include <eosio/eosio.hpp>
#include <string>

using std::string;
using eosio::name;
using eosio::datastream;

namespace prs{

	using namespace eosio;

	class [[eosio::contract]] prsaccount : public contract {
		
	  public:
		prsaccount( name receiver, name code, datastream<const char*> ds )
			   :contract(receiver, code, ds) {}

	    ACTION bind(name   user, 
	                string payment_provider,
		    	    string payment_account, 
		   			string meta,
		   			string memo);

		using bind_action  = action_wrapper<"bind"_n,  &prsaccount::bind>;

	}; //class
}