#include <prs.account/prs.account.hpp>

namespace prs{

	using std::string;
	using namespace eosio;

	ACTION prsaccount::bind(name user, 
	                        string payment_provider,
		    	            string payment_account, 
		   			        string meta,
		   		 	        string memo){
			  print("prsaccount::bind called");
			  require_auth (_self);
			}
} 
