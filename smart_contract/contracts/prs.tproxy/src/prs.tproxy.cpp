#include <prs.tproxy/prs.tproxy.hpp>
#include <eosio/system.hpp>
#include <eosio/transaction.hpp>
#include <eosio/crypto.hpp>

namespace prs{

	using std::string;
	using namespace eosio;

	ACTION tproxy::reqdeposit(name     user, 
							  uint64_t deposit_id,
							  string   mixin_trace_id,
 							  string   mixin_account_id,
							  asset    amount, 
							  string   memo) {
		print("tproxy::reqdeposit called");
		require_auth(user);
		
		auto idx = _req.get_index<"byuser"_n>();
		auto itr = idx.find(user.value);
		check(itr == idx.end(), "User has submited request, wait till the current request finished");

		auto sym = amount.symbol;
		check (sym.is_valid(), "invalid symbol name");
		check (amount.is_valid(), "invalid amount");
		check (amount.amount > 0, "amount must be positive");

		_req.emplace(_self, [&](auto& obj) {
				obj.req_id           = _req.available_primary_key() + get_current_block_time();
				obj.mixin_trace_id   = mixin_trace_id;
				obj.mixin_account_id = mixin_account_id;
				obj.type             = DEPOSIT;
				obj.status           = REQUESTED;
				obj.user             = user; 
				obj.amount           = amount; 	
				obj.memo             = memo;
				obj.time_stamp_req   = get_current_block_time();
				obj.trx_id_req       = get_trx_id();
		});		
	}
	
	/*
	user account should grant "active" permission to prs.tproxy to run this API:
	 
	 	cleos set account permission <USER_ACCOUNT> active data.json owner -p <USER_ACCOUNT>

	example:
		test.bp2 grant prs.tpxoy eosio.code permission:

	cleos --url http://51.68.201.144:8888 set account permission \
	      test.bp2 active \
		  '{"threshold": 1, \
		  "keys":[{"key": "EOS7ZtBTNsekNJWJHMieEeELQWPpVwbKs7ezVE9nKk9rpkJ69wxUR","weight": 1}], \
		  "accounts": [{"permission":{"actor": "prs.tproxy", "permission": "eosio.code"}, \
		  "weight": 1}]}'\
		  owner -p test.bp2@active
	*/

	ACTION tproxy::reqwithdraw(name     user,
							   uint64_t withdraw_id,
							   string   mixin_trace_id,
							   string   mixin_account_id,
		                 	   asset    amount, 
							   string   memo) {
		print("tproxy::reqwithdraw called");		
		require_auth(user);
		
		auto idx = _req.get_index<"byuser"_n>();
		auto itr = idx.find(user.value);
		check(itr == idx.end(), "User has submited request, wait till the current request finished");

		auto sym = amount.symbol;
		check ( sym.is_valid(), "invalid symbol name");
		check ( amount. is_valid(), "invalid amount");
		check ( amount.amount > 0, "amount must be positive"); 	

		//transfer token from user account to prs.tproxy
		action(
			permission_level{user, "active"_n},
			"eosio.token"_n,
			"transfer"_n,
			std::make_tuple(user,
							"prs.tproxy"_n,
							asset(amount.amount, amount.symbol), 
							std::string(""))
		).send();

		_req.emplace(_self, [&](auto& obj) {
				obj.req_id           = withdraw_id;
				obj.mixin_trace_id   = mixin_trace_id;
				obj.mixin_account_id = mixin_account_id;
				obj.type             = WITHDRAW;
				obj.status           = REQUESTED;
				obj.user             = user; 
				obj.amount           = amount; 	
				obj.memo             = memo;
				obj.time_stamp_req   = get_current_block_time();
				obj.trx_id_req       = get_trx_id();
		});		
	}

	ACTION tproxy::sync(uint64_t req_id,
						bool     sync_result,
						string   sync_meta, 
						string   memo) {
		print("tproxy::sync called");
		require_auth(_self);

        auto req_iter = _req.find(req_id);
		check (req_iter != _req.end(), "invalid req_id");	

		if (sync_result) {
			bool remove_req = false;

			_req.modify(req_iter, _self, [&](auto &upd){
				
				check(upd.status == REQUESTED, "invalid request status");

				if (upd.type == DEPOSIT) {
					//send certain amount of token to tprxy.oracle
					action(
						permission_level{"prs.tproxy"_n, "active"_n},
						"eosio.token"_n,
						"transfer"_n,
						std::make_tuple(get_self(),
										"tprxy.oracle"_n,
										asset(upd.amount.amount, upd.amount.symbol),
										std::string(""))
					).send();

					//add deposit transfer recorder to tprxy.oracle
					action(
						permission_level{"prs.tproxy"_n, "active"_n},
						"tprxy.oracle"_n,
						"adddeposit"_n,
						std::make_tuple(get_self(),
										upd.req_id,
										upd.mixin_trace_id,
										upd.user,
										asset(upd.amount.amount, upd.amount.symbol),
										std::string(""))
					).send();

					//add auth request to tprxy.oracle
					action (
						permission_level{get_self(), "active"_n},
						"tprxy.oracle"_n,
						"addauthreq"_n,
						std::make_tuple(get_self(),
										upd.req_id, 
										upd.mixin_trace_id,
										sync_meta,
										std::string(""))
					).send();				

					//set auth result to false, wait oracle auth result
					upd.status      = WAIT_ORACLE;
					upd.auth_result = false;
					//update request status   
					upd.sync_content    = sync_meta;			
					upd.time_stamp_sync = get_current_block_time();
					upd.trx_id_sync     = get_trx_id();		
				} else if (upd.type == WITHDRAW) {
					// sync service can be trusted, no need for oracle auth
					// for withdraw, after receive successful sync message from sync server,
					// withdraw request with same id will be deleted from req table
					upd.status      = SUCCESS;
					upd.auth_result = true;
					remove_req = true;
				}						
			});

			if (remove_req) {
				_req.erase(req_iter);
			}
		} else {
			// sync failed
			// for withdraw, send balance back to user account, remove related recorder
			// user should start request new withdraw from payment service
			// for deposit, delete user req
			_req.modify(req_iter, _self, [&](auto &upd){
				check(upd.status == REQUESTED, "invalid request status");
				if (upd.type == WITHDRAW) {
					//transfer certain amount token from prs.tproxy back to user account
					action(
						permission_level{"prs.tproxy"_n, "active"_n},
						"eosio.token"_n,
						"transfer"_n,
						std::make_tuple("prs.tproxy"_n,
										upd.user,
										asset(upd.amount.amount, upd.amount.symbol), 
										std::string(""))
					).send();	

				} else {
					print("{DEPOSIT SYNC FAILED, REMOVE REQUEST}");					
				}
			});
			_req.erase(req_iter);
		}
	}

	ACTION tproxy::updauth(name     oracle,
						   uint64_t req_id,
						   bool     auth_result, 
						   string   auth_data, 
                           string   memo) {
		print("tproxy::updauth called");

		require_auth("tprxy.oracle"_n);
	
        auto req_iter = _req.find(req_id);

		if (req_iter == _req.end()) {
			check(false, "request_id is not exist");
		}
		
		_req.erase(req_iter);
		print("req deleted");
	}

	ACTION tproxy::cancelreq(name user, string memo) {
		print("tproxy::cancelreq called");
		require_auth(user);

		auto idx = _req.get_index<"byuser"_n>();
		auto itr = idx.find(user.value);
		check(itr != idx.end(), "User has no request to cancel");
		
		bool can_cancel = false;
		idx.modify(itr, _self, [&](auto &upd){
			if (upd.type == DEPOSIT && upd.status == REQUESTED) {
				can_cancel = true;
			}
		});

		if (can_cancel) {
			idx.erase(itr);
			print("user request canceled");			
		} else {
			check(false, "CANNOT CANCEL REQUEST");
		}		
	}

	//test only
	ACTION tproxy::purgeall(string memo) {
		print("tproxy::purgeall called");
		require_auth(_self);

		// purge all requests       
		for(auto itr = _req.begin(); itr != _req.end();) {
            itr = _req.erase(itr);        
        }		 	
	}	

	ACTION tproxy::setdailyl(uint64_t amount) {
		print("tproxy::cancelreq called");
		require_auth("eosio"_n);
		check (amount > 0, "daily withdraw amount can not be negative");
		_config.daily_withdraw_limit = amount;
		_gconfig.set(_config, get_self());
	}

	tproxy::prs_tproxy_config tproxy::get_default_parameters() {
		prs_tproxy_config  config;
		config.daily_withdraw_limit = default_daily_withdraw_limit;
		return config;
	}

	checksum256 tproxy::get_trx_id() {
    	size_t size = transaction_size();
    	char buf[size];
    	size_t read = read_transaction( buf, size );
    	check( size == read, "read_transaction failed");
    	return sha256( buf, read );
	}

	uint32_t tproxy::get_current_block_time() {
		eosio::time_point now = eosio::current_block_time(); 
        uint32_t time_stamp = now.sec_since_epoch();
		return time_stamp;
	}
} //namespace
