#pragma once

#include <eosio/eosio.hpp>
#include <eosio/asset.hpp>
#include <eosio/system.hpp>
#include <eosio/singleton.hpp>
#include <string>

using std::string;
using eosio::name;
using eosio::datastream;
using eosio::asset;
using eosio::current_time_point;

namespace prs{

	using namespace eosio;
   
	static constexpr uint32_t seconds_per_hour             = 3600;
    static constexpr int64_t  seconds_per_day              = int64_t(seconds_per_hour) * 24;
	static constexpr uint64_t default_daily_withdraw_limit = 500000;

	enum TASK_TYPE {
		DEPOSIT  = 1, 
		WITHDRAW = 2
	};

	enum TASK_STATUS {
		REQUESTED   = 1, 
		WAIT_ORACLE = 2,
		SUCCESS     = 3, 
		FAILED      = 4
	};
	
	class [[eosio::contract]] tproxy : public contract {
		
	  public:
		tproxy( name receiver, name code, datastream<const char*> ds )
			   :contract(receiver, code, ds), 
			   _req(receiver, receiver.value),			   
			   _gconfig(receiver, receiver.value) {
				   _config = _gconfig.exists() ? _gconfig.get() : get_default_parameters();
				   _gconfig.set(_config, get_self());
			   }
		~tproxy() {
			_gconfig.set(_config, get_self());
		}


		//Actions				
		ACTION reqdeposit(name        user,
						  uint64_t    deposit_id,
						  std::string mixin_trace_id,
						  std::string mixin_account_id,
						  asset       amount,						         
                          std::string memo);
								  
		
		ACTION reqwithdraw(name        user,
						   uint64_t    withdraw_id,
						   std::string mixin_trace_id,
						   std::string mixin_account_id,
						   asset       amount,
						   std::string memo);

	    ACTION sync(uint64_t    req_id,
				    bool        sync_result,
				    std::string sync_meta,
					std::string memo);					

		ACTION updauth(name        sender, 
					   uint64_t    req_id, 
					   bool        auth_result,
					   std::string auth_meta, 
					   std::string memo);

		ACTION purgeall(std::string memo);

		ACTION cancelreq(name user, std::string memo);

		ACTION setdailyl(uint64_t amount);
			
		//Action Wrappers
		using reqdeposit_action  = action_wrapper<"reqdeposit"_n,  &tproxy::reqdeposit>;
		using reqwithdraw_action = action_wrapper<"reqwithdraw"_n, &tproxy::reqwithdraw>;	
		using sync_action        = action_wrapper<"sync"_n,        &tproxy::sync>;
		using updauth_action     = action_wrapper<"updauth"_n,     &tproxy::updauth>;
		using purgeall_action    = action_wrapper<"purgeall"_n,    &tproxy::purgeall>;
		using cancelreq_action   = action_wrapper<"cancelreq"_n,   &tproxy::cancelreq>;
		using setdailyl_action   = action_wrapper<"setdailyl"_n,   &tproxy::setdailyl>;
		
	  private:

        struct  [[eosio::table]]  reqitems {
			uint64_t          req_id;
			std::string       mixin_trace_id;
			std::string       mixin_account_id;
			name              user;
            uint8_t           type;
		    uint8_t           status;          
            asset             amount;
			std::string       memo;
			std::string       sync_content;
			std::string       auth_content;
			bool              auth_result;
			uint32_t          time_stamp_req  = 0;
			uint32_t          time_stamp_sync = 0;
			uint32_t          time_stamp_auth = 0; 			 
			checksum256       trx_id_req;
			checksum256       trx_id_sync;
			checksum256       trx_id_auth;

            uint64_t primary_key() const { return req_id; }
			uint64_t by_user_account() const { return user.value; }
        };

		struct [[eosio::table("config")]] prs_tproxy_config { 
			prs_tproxy_config() { }    
			
			uint64_t  daily_withdraw_limit;		
			EOSLIB_SERIALIZE(prs_tproxy_config, (daily_withdraw_limit))
		};
 
        using req_index = eosio::multi_index<"req"_n, reqitems, 
			indexed_by<"byuser"_n, const_mem_fun<reqitems, uint64_t, &reqitems::by_user_account>>>;		

		using config_singleton = eosio::singleton< "config"_n, prs_tproxy_config>;

		//multi_index table
		req_index         _req;
		config_singleton  _gconfig;
		prs_tproxy_config _config;

		static prs_tproxy_config get_default_parameters();

		checksum256 get_trx_id();

		uint32_t get_current_block_time();
			
	};
}