#include <prs.nft/prs.nft.hpp>

namespace prs{

	using std::string;
	using namespace eosio;

	ACTION pnft::create(name issuer, string sym, string memo){
		require_auth(_self);
		check(is_account(issuer), "Issuer account does not exist");

		asset supply(0, symbol(symbol_code(sym.c_str()), 0));
		auto symbol = supply.symbol;
		check(symbol.is_valid(), "invalid symbol name");

		auto symbol_name = symbol.code().raw();
		currency_index  currency_table(_self, symbol_name);
		auto existing_currency = currency_table.find(symbol_name);
		check(existing_currency == currency_table.end(), "token with symbol already exists");

		currency_table.emplace(_self, [&](auto& currency){
			currency.supply = supply;
			currency.issuer = issuer;
		});
	}

	ACTION pnft::issue(name to, asset quantity, vector<string> uris, string token_name, string memo){
		check(is_account(to), "invalid to account");

 		auto symbol = quantity.symbol;
        check( symbol.is_valid(), "invalid symbol name" );
        check( symbol.precision() == 0, "quantity must be a whole number" );
        check( memo.size() <= 256, "memo has more than 256 bytes" );

		check( token_name.size() <= 32, "name has more than 32 bytes" );

        auto symbol_name = symbol.code().raw();
        currency_index currency_table( _self, symbol_name );
        auto existing_currency = currency_table.find( symbol_name );
        check( existing_currency != currency_table.end(), "token with symbol does not exist. create token before issue" );
        const auto& st = *existing_currency;

        // Ensure have issuer authorization and valid quantity
        require_auth( st.issuer );
        check( quantity.is_valid(), "invalid quantity" );
        check( quantity.amount > 0, "must issue positive quantity of NFT" );
        check( symbol == st.supply.symbol, "symbol precision mismatch" );

		// Increase supply
		add_supply( quantity );
   		check( quantity.amount == uris.size(), "mismatch between number of tokens and uris provided" );

        for(auto const& uri: uris) {
            mint(to, st.issuer, asset{1, symbol}, uri, token_name);
        }

        add_balance(to, quantity, st.issuer );	
	}	

	ACTION pnft::burn(name owner, id_type token_id, string memo){
		require_auth(owner);
        auto burn_token = _tokens.find(token_id);
		check(burn_token != _tokens.end(), "token with id does not exist");
		check(burn_token->owner == owner, "token not owned by account");

		asset burnt_supply = burn_token->value;

		// Remove token from tokens table
        _tokens.erase(burn_token);

        // Lower balance from owner
        sub_balance(owner, burnt_supply);

        // Lower supply from currency
        sub_supply(burnt_supply);
	}  

	ACTION pnft::transid(name from, name to, id_type id, string memo){
  		check(from != to, "cannot transfer to self");
        require_auth( from );

		check(is_account(to), "to account does not exist");
		check(memo.size() <= 256, "memo has more than 256 bytes");

		auto send_token = _tokens.find( id );
        check(send_token != _tokens.end(), "token with specified ID does not exist");
        check(send_token->owner == from, "sender does not own token with specified ID");

		const auto& st = *send_token;

		// Notify both recipients
        require_recipient(from);
        require_recipient(to);

        _tokens.modify(send_token, from, [&]( auto& token ) {
	        token.owner = to;
        });

		// Change balance of both accounts
        sub_balance(from, st.value);
        add_balance(to, st.value, from);
	}

	ACTION pnft::transquant(name from, name to, asset quantity, string memo){
	}	

	void pnft::mint(name owner,name ram_payer, asset value, string uri, string token_name){
		_tokens.emplace(ram_payer, [&](auto& token){
			token.id = _tokens.available_primary_key();
			token.uri = uri;
			token.owner = owner;
			token.value = value;
			token.tokenName = token_name;
		});
	}

	void pnft::setrampayer(name payer, id_type id) {
		require_auth(payer);

		// Ensure token ID exists
		auto payer_token = _tokens.find( id );
		check( payer_token != _tokens.end(), "token with specified ID does not exist" );

		// Ensure payer owns token
		check( payer_token->owner == payer, "payer does not own token with specified ID");

		const auto& st = *payer_token;

		// Notify payer
		require_recipient(payer);

		// Set owner as a RAM payer
		_tokens.modify(payer_token, payer, [&](auto& token){
			token.id = st.id;
			token.uri = st.uri;
			token.owner = st.owner;
			token.value = st.value;
			token.tokenName = st.tokenName;
		});

		sub_balance(payer, st.value);
		add_balance(payer, st.value,payer);
	}

	void pnft::add_balance(name owner, asset value, name ram_payer){
		account_index to_accounts( _self, owner.value );
        auto to = to_accounts.find( value.symbol.code().raw() );
        if( to == to_accounts.end() ) {
            to_accounts.emplace( ram_payer, [&]( auto& a ){
                a.balance = value;
            });
        } else {
            to_accounts.modify( to, _self, [&]( auto& a ) {
                a.balance += value;
            });
        }
	}
	
	void pnft::sub_balance(name owner, asset value){
		account_index from_acnts( _self, owner.value );
        const auto& from = from_acnts.get( value.symbol.code().raw(), "no balance object found" );
        check( from.balance.amount >= value.amount, "overdrawn balance" );

        if( from.balance.amount == value.amount ) {
            from_acnts.erase( from );
        } else {
            from_acnts.modify( from, owner, [&]( auto& a ) {
                a.balance -= value;
            });
        }
	}

	void pnft::add_supply(asset quantity){
        auto symbol_name = quantity.symbol.code().raw();
        currency_index currency_table( _self, symbol_name );
        auto current_currency = currency_table.find( symbol_name );

        currency_table.modify( current_currency, name(0), [&]( auto& currency ) {
            currency.supply += quantity;
        });
	}

	void pnft::sub_supply(asset quantity){
		auto symbol_name = quantity.symbol.code().raw();
        currency_index currency_table( _self, symbol_name );
        auto current_currency = currency_table.find( symbol_name );

        currency_table.modify( current_currency, _self, [&]( auto& currency ) {
            currency.supply -= quantity;
        });
	}	
} //namespace
