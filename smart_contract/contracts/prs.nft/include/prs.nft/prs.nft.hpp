#pragma once

#include <eosio/eosio.hpp>
#include <eosio/asset.hpp>
#include <string>
#include <vector>

using eosio::name;
using eosio::datastream;
using eosio::asset;
using std::vector;
using std::string;

typedef uint128_t uuid;
typedef uint64_t id_type;
typedef std::string uri_type;

namespace prs{

	using namespace eosio;

	class [[eosio::contract]] pnft : public contract {
		
	  public:
		pnft( name receiver, name code, datastream<const char*> ds )
			   :contract(receiver, code, ds),
			    _tokens(receiver, receiver.value) {}

		ACTION create(name        issuer, 
		              std::string symbol, 
					  std::string memo);

		ACTION issue(name to,
					 asset quantity,
				     vector<std::string> uris,
					 std::string token_name,
					 std::string memo);

		ACTION burn(name owner,
					id_type token_id,
					std::string  memo);					  

		ACTION transid(name from, 
					   name to,
					   id_type id,
					   std::string memo);

		ACTION transquant(name from,
                          name to,
                          asset quantity,
                          std::string memo);	

		ACTION setrampayer(name payer,
						   id_type id);


		//Action Wrappers
		using create_action      = action_wrapper<"create"_n,      &pnft::create>;
		using issue_action       = action_wrapper<"issue"_n,       &pnft::issue>;
		using burn_action        = action_wrapper<"burn"_n,        &pnft::burn>;
		using transid_action     = action_wrapper<"transid"_n,     &pnft::transid>;	
		using transquant_action  = action_wrapper<"transquant"_n,  &pnft::transquant>;
		using setrampayer_action = action_wrapper<"setrampayer"_n, &pnft::setrampayer>;	
	
	  private:

        struct  [[eosio::table]]  account {
			asset balance;
            uint64_t primary_key() const { return balance.symbol.code().raw(); }
        };

		struct [[eosio::table]] stat  {
			asset supply;
			name issuer;

            uint64_t primary_key() const { return supply.symbol.code().raw(); }
			uint64_t get_issuer() const {return issuer.value;}
		};

       struct [[eosio::table]] token {
            id_type id;          // Unique 64 bit identifier,
            uri_type uri;        // RFC 3986
            name owner;  	     // token owner
            asset value;         // token value
	    	string tokenName;	 // token name

            id_type primary_key() const { return id; }
            uint64_t get_owner() const { return owner.value; }
            string get_uri() const { return uri; }
            asset get_value() const { return value; }
	    	uint64_t get_symbol() const { return value.symbol.code().raw(); }
	    	string get_name() const { return tokenName; }

			// generated token global uuid based on token id and
			// contract name, passed in the argument
			uuid get_global_id(name self) const
			{
				uint128_t self_128 = static_cast<uint128_t>(self.value);
				uint128_t id_128 = static_cast<uint128_t>(id);
				uint128_t res = (self_128 << 64) | (id_128);
				return res;
			}

			string get_unique_name() const
			{
				string unique_name = tokenName + "#" + std::to_string(id);
				return unique_name;
			}
        };


        typedef multi_index<"accounts"_n, account> account_index;
		typedef multi_index<"stats"_n,    stat>    currency_index;
		typedef multi_index<"tokens"_n,   token>   token_index;

		private:
			token_index _tokens;
			void mint(name owner, name ram_payer, asset value, string uri, string name);
			void add_balance(name owner, asset value, name ram_payer);
			void sub_balance(name owner, asset value);
			void add_supply(asset quantity);
			void sub_supply(asset quantity);

	}; //class
}