# prs.nft Contract

test host: 51.255.133.170:8888
contract account : prs.nft
key:
    Private key: 5HqMXqNajo3XFqSEAikbkh7MCzjLm7uS1Hn347yiFDKQ5T2DZFQ
    Public key: EOS6Q3gyyiKJHHXq1v9DxLjWthYAafTzJ6H46kSczXnnmUzD16SMC

actions
    
    	ACTION create(name        issuer, 
		              std::string symbol, 
					  std::string memo);

        用途：
            用来创建一个NFT symbol
        
        参数：
            issuer  : 发行者，规定谁可以“发行”这种token
            symbol : NFT 名称，该名称必须符合EOS token命名规范
            memo   : memo， 不能超过256个字符

        权限：
            需要合约所有者的action权限，即需要prs.nft签名

        说明：
            在发行（issue）一个真正的nft代币之前，需要先通过create创建这种token（的名称），创建时规定谁可以发行这种token

        举例：

            创建一种名称是 “PUREART”的NFT
                cleos  --url http://51.255.133.170:8888 push action prs.nft create '["testuser1", "PUREART", "memo"]' -p prs.nft@active

            查看结果：
                cleos  --url http://51.255.133.170:8888 get table prs.nft PUREART stats

                结果：
                {
                    "rows": [{
                        "supply": "0 PUREART",
                        "issuer": "testuser1"
                        }
                    ],
                    "more": false
                }

                其中supply表示该nft代币的供应量，issuer表示发行者

		ACTION issue(name to,
					 asset quantity,
				     vector<std::string> uris,
					 std::string token_name,
					 std::string memo);

        用途：
            发行者发行一定数量的nft代币
        参数：
            to:发行给谁
            quantity:发行数量
            uris:每个代币表示的uri（可以指向任何需要的内容）
            token_name: token的名字，一个symbol下的每个token都可以有不同（或相同）的名字，比如叫"房产证",
            注意：同一次issue生的所有token都是一个名字
            memo:   memo，不能超过256个字符

        说明：token_name可以按照实际需要填写，这里名字和创建时的symbol名字关如下，例如create的symbol可以叫“艺术品币”，然后可以用“艺术品币”这个symbol来issue 4个叫“蒙娜丽莎”的独有token，每个token代表了画的1/4。那么在issue的时候，token_name就要填“蒙娜丽莎”。每个token需要指向一个独有的uri，以证明这个token代表的内容，如果不需要URI，可以填"NAN"，但是要求uris的数量必须等于quantity的数量

        举例：
            创建4个"Mona Lisa" PUREART nft并转给 testuser1

                cleos  --url http://51.255.133.170:8888 push action prs.nft issue '{"to":"testuser1", "quantity":"4 PUREART", "uris":["uri1","uri2", "uri3","uri4"], "token_name":"Mona Lisa","memo":"memo"}' -p testuser1@active

            查看结果：

                查看PUREART代币的总供应量
                cleos  --url http://51.255.133.170:8888 get table prs.nft PUREART stats
                
                可以看到供应量增加到了4
                {
                "rows": [{
                    "supply": "4 PUREART",
                    "issuer": "testuser1"
                    }
                ],
                "more": false
                }

                查看刚刚创建的token（注意：每个token都是独一无二的）
                cleos  --url http://51.255.133.170:8888 get table prs.nft prs.nft tokens

                {
                    "rows": [{
                        "id": 0,
                        "uri": "uri1",
                        "owner": "testuser1",
                        "value": "1 PUREART",
                        "tokenName": "Mona Lisa"
                        },{
                        "id": 1,
                        "uri": "uri2",
                        "owner": "testuser1",
                        "value": "1 PUREART",
                        "tokenName": "Mona Lisa"
                        },{
                        "id": 2,
                        "uri": "uri3",
                        "owner": "testuser1",
                        "value": "1 PUREART",
                        "tokenName": "Mona Lisa"
                        },{
                        "id": 3,
                        "uri": "uri4",
                        "owner": "testuser1",
                        "value": "1 PUREART",
                        "tokenName": "Mona Lisa"
                        }
                    ],
                    "more": false
                }

                查看testuser1的balance
                cleos  --url http://51.255.133.170:8888 get table prs.nft testuser1 accounts

                可以看到testuser1拥有4个PUREART nft
                {
                "rows": [{
                    "balance": "4 PUREART"
                    }
                ],
                "more": false
                }

		ACTION burn(name owner,
					id_type token_id,
					std::string  memo);	

        用途：
            用户销毁一个nft token
        参数：
            owner ：所有者
            token_id : 要销毁nft的id
            memo ：memo，不能超过256个字符

        说明：
            用户可以销毁一个创建的nft

        举例：
            testuser1销毁id是“0”的nft
                cleos  --url http://51.255.133.170:8888 push action prs.nft burn '["testuser1", 0, "memo"]' -p testuser1@active
            
            查看PUREART代币的总供应量
                cleos  --url http://51.255.133.170:8888 get table prs.nft PUREART stats
                
                可以看到供应量减少到了3
                {
                "rows": [{
                    "supply": "3 PUREART",
                    "issuer": "testuser1"
                    }
                ],
                "more": false
                }

            查看token
                cleos  --url http://51.255.133.170:8888 get table prs.nft prs.nft tokens
                id是0的token已经被销毁（删除）
                {
                    "rows": [{
                        "id": 1,
                        "uri": "uri2",
                        "owner": "testuser1",
                        "value": "1 PUREART",
                        "tokenName": "Mona Lisa"
                        },{
                        "id": 2,
                        "uri": "uri3",
                        "owner": "testuser1",
                        "value": "1 PUREART",
                        "tokenName": "Mona Lisa"
                        },{
                        "id": 3,
                        "uri": "uri4",
                        "owner": "testuser1",
                        "value": "1 PUREART",
                        "tokenName": "Mona Lisa"
                        }
                    ],
                    "more": false
                }

            查看testuser1的balance
                cleos  --url http://51.255.133.170:8888 get table prs.nft testuser1 accounts

                可以看到testuser1拥有3个PUREART nft
                {
                "rows": [{
                    "balance": "3 PUREART"
                    }
                ],
                "more": false
                }            


		ACTION transid(name from, 
					   name to,
					   id_type id,
					   std::string memo);
        用途：
            将一个特定id的nft从from账户转到to账户

        参数：
            from : 转出账户
            to : 转入账户
            id: token id
            memo : memo 不能超过256个字符
        
        说明：
            等同于普通代币的转账功能

        举例：
            testuser1将id为“1”的nft划转给testuse2
            cleos  --url http://51.255.133.170:8888 push action prs.nft transid '["testuser1", "testuser2", 1, "memo"]' -p testuser1@active

           查看testuser1的账户
           cleos  --url http://51.255.133.170:8888 get table prs.nft testuser1 accounts

            {
            "rows": [{
                "balance": "2 PUREART"
                }
            ],
            "more": false
            }

           查看testuser2的账户
           cleos  --url http://51.255.133.170:8888 get table prs.nft testuser2 accounts
            {
            "rows": [{
                "balance": "1 PUREART"
                }
            ],
            "more": false
            }

            查看token归属，可以看到id为1的PUREART已经属于testuser2
            cleos  --url http://51.255.133.170:8888 get table prs.nft prs.nft tokens
            {
            "rows": [{
                "id": 1,
                "uri": "uri2",
                "owner": "testuser2",
                "value": "1 PUREART",
                "tokenName": "Mona Lisa"
                },{
                "id": 2,
                "uri": "uri3",
                "owner": "testuser1",
                "value": "1 PUREART",
                "tokenName": "Mona Lisa"
                },{
                "id": 3,
                "uri": "uri4",
                "owner": "testuser1",
                "value": "1 PUREART",
                "tokenName": "Mona Lisa"
                }
            ],
            "more": false
            }

		ACTION setrampayer(name payer,
						   id_type id)
        用途：
            更改某个token的内存支付者(mem payer)
        参数：
            payer ：支付者
            id : 要支付的nft id
        说明：
            EOS特有的问题，一个token在创建时，因为占用了表格里的存储空间，所以需要创建者支付内存费用，在转让后，应该由新的拥有者支付内存费，该API为可选
        
        举例：
            testuser2支付刚刚得到的id为1的nft PUREART的存储费用
            
            cleos  --url http://51.255.133.170:8888 push action prs.nft setrampayer '["testuser2", 1, "memo"]' -p testuser2@active
            


                    


