#pragma once

#include <eosio/eosio.hpp>
#include <string>

using std::string;
using eosio::name;
using eosio::datastream;

namespace prs{

	using namespace eosio;

	class [[eosio::contract]] pressone : public contract {
		
	  public:
		pressone( name receiver, name code, datastream<const char*> ds )
			   :contract(receiver, code, ds) {}

	    ACTION save(name user, 
	                string id,
		    	    string user_address, 
		   			string type,
		   			string meta,
		   			string data,
		   			string hash, 
					string signature);

		using save_action  = action_wrapper<"save"_n,  &pressone::save>;

	}; //class
}