#include <pressone/pressone.hpp>

namespace prs{

	using std::string;
	using namespace eosio;

	ACTION pressone::save(name user, 
	            string id,
		    	string user_address, 
		   		string type,
		   		string meta,
		   		string data,
		   		string hash, 
				string signature){

			  require_auth (user);
              require_recipient(user);					
			}
} //namespace
