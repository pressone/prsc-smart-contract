#pragma once

#include <eosio/eosio.hpp>
#include <eosio/asset.hpp>
#include <eosio/system.hpp>
#include <eosio/singleton.hpp>
#include <string>

using std::string;
using eosio::name;
using eosio::datastream;
using eosio::asset;
using eosio::current_time_point;

namespace prs{

	using namespace eosio;
   
	static constexpr uint32_t ms_per_10_minutes  = 600 * 1000;   
	static constexpr uint32_t seconds_per_hour   = 3600;
    static constexpr int64_t  seconds_per_day    = int64_t(seconds_per_hour) * 24;
    static constexpr float    default_price      = 0.0001f;
	static constexpr uint64_t sec_per_10_minutes = 600;

	enum TASK_STATUS {
		REQUESTED   = 1, 
		PROMISED    = 2,
		VERIFIED    = 3, 
		EXPIRED     = 3, 
		FAILED      = 4
	};
	
	class [[eosio::contract]] ipfs : public contract {
		
	  public:
		ipfs( name receiver, name code, datastream<const char*> ds )
			   :contract(receiver, code, ds) {} //, 
			  // _file(receiver, receiver.value),
			  // _slice(receiver, receiver.value),			   
			  // _promise(receiver, receiver.value),
			  // _block(receiver, receiver.value),
			  // _verify(receiver, receiver.value),			   
			  //  _gconfig(receiver, receiver.value) {
			  //	   _config = _gconfig.exists() ? _gconfig.get() : get_default_parameters();
			  //	   _gconfig.set(_config, get_self());
			  // }

		~ipfs() {
			//_gconfig.set(_config, get_self());
		}

		// Action
		// addfile
		// parameters: 
		//  ** user           : client eos account name
		//  ** ipfs_id        : client ipfs id
		//  ** TBD

		ACTION addfile(name                     user,
					   std::string              ipfs_id,
					   //meta
					   std::vector<std::string> uri,
					   std::string              mime,
					   std::string              filename,
					   std::string              extname,
					   std::string              hash_alg,
					   uint64_t                 size,
					   std::string              encry_alg,
					   std::string              encry_key,
					   //data 
					   std::string              hash);
		
		// Action
		// addslic
		// parameters: 
		//  ** user           : client eos account name
		//  ** ipfs_id        : client ipfs id
		//  ** TBD	

		ACTION addslice(name        user,
					    std::string user_address,
						//meta
						std::string publish_id,						
						uint64_t    size,
						//data
						uint64_t    period,
						std::vector<std::string> slices);

		// Action 
		// addpromise

		ACTION addpromise(name        user,
		     		      std::string ipfs_id,
                          //meta
						  uint64_t    size,
						  std::string publish_id,    
						  std::string ipfs_signature,
						  //data
						  std::string request,
						  std::string ipfs_peer_id,
						  std::string ipfs_peer_publickey,
						  name        user_account,
						  std::string period_of_begin,
						  std::string period_of_end,
						  std::vector<std::string> slices);

		// Action
		// addverify
		ACTION addverify(name user,
		                 // meta 
						 uint64_t size,
						 std::string publish_id,
						 std::string ipfs_signatur,
						 //data 
						 std::string promise, 
						 std::string ipfs_peer_id,
						 //std::vector<std::string> uri,
						 std::string result);

		// Action
		// addblockinfo
		ACTION addblockinfo(name        oracle, 
		                    std::string block_hash,
							uint64_t    timestamp,
							std::string memo);

		// Action
		// prugeblockinfo
		// ** dev only
		ACTION purgeblockinf(name        oracle,
						     std::string memo);

		//Action Wrappers
		using addfile_action        = action_wrapper<"addfile"_n,       &ipfs::addfile>;
		using addslice_action       = action_wrapper<"addslice"_n,      &ipfs::addslice>;	
		using addpromise_action     = action_wrapper<"addpromise"_n,    &ipfs::addpromise>;
		using addverify_action      = action_wrapper<"addverify"_n,     &ipfs::addverify>;
		using addblock_action       = action_wrapper<"addblockinfo"_n,  &ipfs::addblockinfo>;
		using purgeblock_inf_action = action_wrapper<"purgeblockinf"_n, &ipfs::purgeblockinf>;

	  private:

		/*
        struct  [[eosio::table]] file_item {
			uint64_t                 file_id;
			name                     user;
			std::string              ipfs_id;
			std::vector<std::string> uri;
			std::string              mime;
			std::string              filename;
			std::string              extname;
			std::string              hashalg;
			uint64_t                 size;
			std::string              encry_alg;
			std::string              encry_key;
			std::string              hash;
			uint8_t                  file_status;
			checksum256              trx_id;

            uint64_t primary_key() const { return file_id; }

        };

		struct [[eosio::table]] slice_item {
			uint64_t                 slice_id;	
			name                     user;
			std::string              user_address;
			std::string              publish_id;
			uint64_t                 size;
			uint64_t                 period;
			std::vector<std::string> slices;
			checksum256              trx_id;

			uint64_t primary_key() const { return slice_id; }
		};

		struct [[eosio::table]] promise_item {
			uint64_t    promise_id;
			name        user; 
			std::string ipfs_id;
			uint64_t    size;
			std::string publish_id;  //file_trx_id;
			std::string request_id;  //slice_trx_id;
			std::string ipfs_signature;

			uint64_t primary_key() const { return promise_id; }
		};

		struct [[eosio::table]] verify_item {
			uint64_t verify_id;

			uint64_t primary_key() const { return verify_id; }
		};

		struct [[eosio::table]] block_info_item {
			uint64_t id;
			std::string block_hash_string;
			checksum256 block_hash_sha256;
			uint64_t    block_timestamp;
			uint64_t    primary_key() const { return id; }
			checksum256 by_block_hash_sha256() const { return block_hash_sha256; }
 
		};

		struct [[eosio::table("config")]] prs_ipfs_config { 
			prs_ipfs_config() { }    
			
			float_t  unit_price;
			uint64_t verify_period;

			EOSLIB_SERIALIZE(prs_ipfs_config, (unit_price)(verify_period));
		};

        using file_index       = eosio::multi_index<"file"_n, file_item>;	
		using slice_index      = eosio::multi_index<"slice"_n, slice_item>;
		using promise_index    = eosio::multi_index<"promise"_n, promise_item>;
		using verify_index     = eosio::multi_index<"verify"_n, verify_item>;
		//using block_index      = eosio::multi_index<"blockinfo"_n, block_info_item>;
		using block_index      = eosio::multi_index<"blockinfo"_n, block_info_item, indexed_by<name("idhash"), const_mem_fun<block_info_item, checksum256, &block_info_item::by_block_hash_sha256>>>;
		using config_singleton = eosio::singleton< "config"_n, prs_ipfs_config>;
		
		//multi_index table
		file_index       _file;
		slice_index      _slice;
		promise_index    _promise;
		verify_index     _verify;
		block_index      _block;
		config_singleton _gconfig;
		prs_ipfs_config  _config;
		
		static prs_ipfs_config get_default_parameters();

		checksum256 get_trx_id();		

		uint64_t get_current_block_time();			

		checksum256 hash(std::string str);

		bool check_cert_result(std::string blockId, 
		                       std::string promiseTrxId,
							   std::string publickey);

		*/							   
	};
}