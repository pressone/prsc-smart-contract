#include <prs.ipfs/prs.ipfs.hpp>
#include <eosio/system.hpp>
#include <eosio/transaction.hpp>
#include <eosio/crypto.hpp>
#include <vector>

namespace prs{

	using std::string;
	using std::vector;
	using namespace eosio;

	ACTION ipfs::addfile(name           user,
					     string         ipfs_id,					   
					     vector<string> uri,
					     string         mime,
					     string         filename,
					     string         extname,
					     string         hash_alg,
					     uint64_t       size,
					     string         encry_alg,
					     string         encry_key,
					     string         hash) {
		print("ipfs::addfile called");
		require_auth(user);
	}
	
	ACTION ipfs::addslice(name           user,
					      string         user_address,
				          string         publish_id,						
					      uint64_t       size,
				   	      uint64_t       period,
					      vector<string> slices) {
		print("ipfs::addslice called");		
		require_auth(user);	
	}

	ACTION ipfs::addpromise(name           user,
							string         ipfs_id,						
							uint64_t       size,
							string         publish_id,    
							string         ipfs_signature,
							string         request,
							string         ipfs_peer_id,
							string         ipfs_peer_publickey,
							name           user_account,
							string         period_of_begin,
							string         period_of_end,
							vector<string> slices) {
		print("ipfs::sync called");
		require_auth(user);     
	}

	ACTION ipfs::addverify(name           user,		                 
 						   uint64_t       size,
						   string         publish_id,
						   string         ipfs_signatur,					 
						   string         promise ,
						   string         ipfs_peer_id,
						   //vector<string> uri,
						   string         result) {
		print("ipfs::updauth called");			
		require_auth(user);		
	}

	ACTION ipfs::addblockinfo(name oracle, 
	                          std::string block_hash,
							  uint64_t timestamp,
							  std::string memo) {
	    print("ipfs:addblockinfo called");
		require_auth(oracle);
		
    	check(strcmp(oracle.to_string().c_str(),"ipfs.oracle") == 0, "permission denied");

		/*
		uint64_t current_time = get_current_block_time() / 1000;
		const auto MS_IN_10_MINUTES = minutes(10).count() / 1000;

		for (auto itr = _block.begin(); itr != _block.end();) {
			if ( current_time - itr->block_timestamp >= MS_IN_10_MINUTES ) {
				itr = _block.erase(itr);
			} else {
				break;
			}
		}			

		_block.emplace(_self, [&](auto& new_block_info) {
			new_block_info.id = _block.available_primary_key();
			new_block_info.block_hash_string = block_hash;
			new_block_info.block_hash_sha256 = hash(block_hash);
			new_block_info.block_timestamp = timestamp;
		});

		*/
	}

	ACTION ipfs::purgeblockinf(name oracle, string memo) {
		print("ipfs:addblockinfo called");
		require_auth(oracle);
    	check(strcmp(oracle.to_string().c_str(),"ipfs.oracle") == 0, "permission denied");	

		/*
		for (auto itr = _block.begin(); itr != _block.end();) {
			itr = _block.erase(itr);
		}
		*/
	}

	/*
	ipfs::prs_ipfs_config ipfs::get_default_parameters() {
		prs_ipfs_config  config;
		config.unit_price = default_price;
		config.verify_period = sec_per_10_minutes;
		return config;
	}

	checksum256 ipfs::get_trx_id() {
    	size_t size = transaction_size();
    	char buf[size];
    	size_t read = read_transaction( buf, size );
    	check( size == read, "read_transaction failed");
    	return sha256( buf, read );
	}

	uint64_t ipfs::get_current_block_time() {
		eosio::time_point now = eosio::current_time_point(); 
        eosio::microseconds micros = now.time_since_epoch();
		uint64_t count = micros.count();
		return count;
	}
	
	checksum256 ipfs::hash(string str)
  	{
        return sha256(const_cast<char*>(str.c_str()), str.size());
  	}

	bool ipfs::check_cert_result(string blockId, string promiseTrxId, string publickey) {
		string total;
		return false;
	}

	*/

} //namespace
