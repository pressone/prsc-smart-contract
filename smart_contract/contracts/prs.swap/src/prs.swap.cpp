#include <prs.swap/prs.swap.hpp>
#include <prs.swap/error.hpp>
#include <eosio/system.hpp>
#include <eosio/transaction.hpp>
#include <eosio/crypto.hpp>
#include <eosio/symbol.hpp>

#include <math.h>

namespace prs{

	using std::string;
	using namespace eosio;

	ACTION swap::addpool (name  creator,
						  name  pool_name,
						  asset token1,						
						  asset token2,
						  asset pool_token,
						  string memo){
		print("swap::addpool called");
		require_auth(PRS_SWAP);
		auto pool_iterator = _pools.find(pool_name.value);
	
		check(is_account(creator), "creator does not exist");

		check (pool_iterator == _pools.end(), ERROR_POOL_EXIST);		
		check (token1.is_valid(), ERROR_TOKEN_INVALID);
		check (token2.is_valid(), ERROR_TOKEN_INVALID);
		check (token1.amount > 0, ERROR_TOKEN_INVALID);
		check (token2.amount > 0, ERROR_TOKEN_INVALID);

		_pools.emplace(_self, [&](auto& new_pool) {
			new_pool.pool_name = pool_name;
			new_pool.pool_creator = creator;
			new_pool.pool_status = POOL_OK;
			new_pool.token1 = token1; 
			new_pool.token2 = token2;
			new_pool.pool_token = asset(getll(get_sqrt(token1, token2)), pool_token.symbol);
			new_pool.created_timestamp = get_current_block_time();
		});
	}
	
	ACTION swap::rmpool (name creator,
					     name pool_name,
					     string memo){
		print("swap::rmpool called");
		require_auth(PRS_SWAP);

		auto pool_iterator = _pools.find(pool_name.value);
		check(pool_iterator != _pools.end(), ERROR_POOL_NOT_EXIST);
		if (pool_iterator->pool_creator == creator) {
			_pools.erase (pool_iterator); 
		} else  {
			check (false, ERROR_CREATOR_MISMATCH);
		}		
	}

	ACTION swap::swaptoken (uint64_t req_id,
							name     from_user,
							name     to_user,
				            name     pool_name,
				            asset    amount,
					        uint8_t  slippage,
					        string   mixin_trace_id,
					        string   mixin_account_id,
                            string   memo){
		print("swap::swaptoken called");
		require_auth(from_user);
 		check(is_account(to_user), "to user is not existed");

		auto idx = _trxs.get_index<"byuser"_n>();
		auto itr = idx.find(from_user.value);
		check(itr == idx.end(), "User has submited request, wait till the current request finished");

		auto pool_iterator = _pools.find(pool_name.value);
		check (pool_iterator != _pools.end(),ERROR_POOL_NOT_EXIST);		
		check (pool_iterator->pool_status == POOL_OK, ERROR_INVALID_POOL_STATUS);
		check ((pool_iterator->token1.symbol == amount.symbol || pool_iterator->token2.symbol == amount.symbol), ERROR_TOKEN_INVALID);
		check (amount.is_valid(), ERROR_TOKEN_INVALID);
		check (amount.amount > 0, ERROR_TOKEN_INVALID);
		check ((slippage >= 0 && slippage <=100), "slippage should between 0 and 100 (%)");	

		asset t1 = pool_iterator->token1;
		asset t2 = pool_iterator->token2;

		asset to_token;

		asset minimum_threshold;
		minimum_threshold = asset(1 / (float) 0.003, amount.symbol);		
		check(amount.amount >= minimum_threshold.amount, "insufficient amount, add more token for swap");

		if (amount.symbol == t1.symbol) {
			check(t1.amount > amount.amount, "token overdraw");		
			to_token = get_to_token(t1, t2, amount);
			check (t2.amount > to_token.amount, "token overdraw");
		} else {
			check(t2.amount > amount.amount, "token overdraw");
			to_token = get_to_token(t2, t1, amount);
			check (t1.amount > to_token.amount, "token overdraw");
		}

    	//send collect request to swap.oracle
		send_addcollect(req_id, mixin_trace_id, mixin_account_id, from_user, 
						asset(0, pool_iterator->pool_token.symbol), 
						amount,
						asset(0, to_token.symbol),
						std::string(""),
						std::string("collect token from user"));

		//save swap request to swap table
		_trxs.emplace(_self, [&](auto& new_trx) {
			new_trx.trx_id           = req_id;
			new_trx.from_user        = from_user;
			new_trx.to_user          = to_user;
			new_trx.type             = SWAP;
			new_trx.status           = WAIT_COLLECT;
			new_trx.pool_name        = pool_name;
			new_trx.from_token       = amount;
			new_trx.to_token         = to_token;
			new_trx.final_to_token   = asset(0, to_token.symbol);
			new_trx.fee              = asset(0, amount.symbol);
			new_trx.slippage         = slippage;
			new_trx.req_timestamp    = get_current_block_time();
			new_trx.req_trx_id       = get_trx_id();
			new_trx.mixin_trace_id   = mixin_trace_id;
			new_trx.mixin_account_id = mixin_account_id;
		});		
	}

	ACTION swap::addliquid (uint64_t req_id,
							name user,
						    name pool_name, 
						    asset token1,
						    asset token2,
						    string mixin_trace_id,
						    string mixin_account_id,
						    string memo) {

		print("swap::addliquid called");
		require_auth(user);

		auto idx = _trxs.get_index<"byuser"_n>();
		auto itr = idx.find(user.value);
		check(itr == idx.end(), "User has submited request, wait till the current request finished");

		auto pool_iterator = _pools.find(pool_name.value);
		check (pool_iterator != _pools.end(),ERROR_POOL_NOT_EXIST);		
		check (pool_iterator->pool_status == POOL_OK, ERROR_INVALID_POOL_STATUS);
		check ((pool_iterator->token1.symbol == token1.symbol && pool_iterator->token2.symbol == token2.symbol), ERROR_TOKEN_INVALID);
		check (token1.is_valid(), ERROR_TOKEN_INVALID);
		check (token1.amount > 0, ERROR_TOKEN_INVALID);
		check (token2.is_valid(), ERROR_TOKEN_INVALID);
		check (token2.amount > 0, ERROR_TOKEN_INVALID);
		
		//check ratio of 2 tokens match with tokens in pool
		if (abs ( (float)token1.amount / (float)pool_iterator->token1.amount - (float)token2.amount / (float)pool_iterator->token2.amount) > TOKEN_RATIO_THRESHOLD) {
			check (false, ERROR_TOKEN_RATIO_MISMATCH);
		}
				
    	//send collect request to swap.oracle
		send_addcollect(req_id, mixin_trace_id, mixin_account_id, user, 
						asset(0, pool_iterator->pool_token.symbol),
						token1,
						token2,
						std::string(""),
						std::string("addliquid, collect both tokens from user"));

		asset t1 = asset( (float) token1.amount / (float) pool_iterator->token1.amount * pool_iterator->pool_token.amount, pool_iterator->pool_token.symbol);
		asset t2 = asset( (float) token2.amount / (float) pool_iterator->token2.amount * pool_iterator->pool_token.amount, pool_iterator->pool_token.symbol);

		//save ADD_LIQUID request to trans table
		_trxs.emplace(_self, [&](auto& new_trx) {
			new_trx.trx_id           = req_id;
			new_trx.from_user        = user;
			new_trx.to_user          = user;
			new_trx.type             = ADD_LIQUID;
			new_trx.status           = WAIT_COLLECT;
			new_trx.pool_name        = pool_name;
			new_trx.from_token       = token1;                               //token1
			new_trx.to_token         = token2;                               //token2
			new_trx.pool_token       = asset(t1.amount >= t2.amount ? t1.amount : t2.amount, pool_iterator->pool_token.symbol);     //pool_token amount;
			new_trx.fee              = asset(0, pool_iterator->pool_token.symbol);
			new_trx.slippage         = 0;
			new_trx.req_timestamp    = get_current_block_time();
			new_trx.req_trx_id       = get_trx_id();
			new_trx.mixin_trace_id   = mixin_trace_id;
			new_trx.mixin_account_id = mixin_account_id;
		});
	}

	ACTION swap::rmliquid (uint64_t req_id,
						   name user,
					       name pool_name,
					       asset pool_token,
					       string mixin_trace_id,
					       string mixin_account_id,
					       string memo) {
		print("swap::rmliquid called");
		require_auth(user);
		
		check (pool_token.is_valid(), ERROR_TOKEN_INVALID);
		check (pool_token.amount > 0, ERROR_TOKEN_INVALID);

		auto idx = _trxs.get_index<"byuser"_n>();
		auto itr = idx.find(user.value);
		check(itr == idx.end(), "User has submited request, wait till the current request finished");

		auto pool_iterator = _pools.find(pool_name.value);
		check (pool_iterator != _pools.end(),ERROR_POOL_NOT_EXIST);		
		check (pool_iterator->pool_status == POOL_OK, ERROR_INVALID_POOL_STATUS);
		check (pool_iterator->pool_token.symbol == pool_token.symbol, ERROR_TOKEN_INVALID);

		//transfer token from user account to prs.tproxy
		action(
			permission_level{user, "active"_n},
			"eosio.token"_n,
			"transfer"_n,
			std::make_tuple(user,
						    "prs.swap"_n,
							 pool_token,
							 std::string(""))
		).send();

		asset token1 = asset ( (float) pool_token.amount / (float) pool_iterator->pool_token.amount * pool_iterator->token1.amount, pool_iterator->token1.symbol);
		asset token2 = asset ( (float) pool_token.amount / (float) pool_iterator->pool_token.amount * pool_iterator->token2.amount, pool_iterator->token2.symbol);
		
		//send settle request to swap.oracle
		send_addsettle(req_id, 
					   std::string(mixin_trace_id),
					   std::string(mixin_account_id),
					   user,
					   asset(0, pool_iterator->pool_token.symbol),
					   token1,	 
					   token2,	
					   string(""),
					   string("pay token to user"));
		
		// save transaction req
		_trxs.emplace(_self, [&](auto& new_trx) {
			new_trx.trx_id           = req_id;
			new_trx.from_user        = user;
			new_trx.to_user          = user;
			new_trx.type             = RM_LIQUID;
			new_trx.status           = WAIT_SETTLE;
			new_trx.pool_name        = pool_name;
			new_trx.pool_token       = pool_token;
			new_trx.from_token       = token1;         //to:   token1
			new_trx.to_token         = token2;         //to:   token2
			new_trx.slippage         = 0;
			new_trx.req_timestamp    = get_current_block_time();
			new_trx.req_trx_id       = get_trx_id();
			new_trx.mixin_trace_id   = mixin_trace_id;
			new_trx.mixin_account_id = mixin_account_id;
		});
	}

	ACTION swap::cancel (uint64_t    req_id,
						 name        user,	
					     name        pool_name,		
					     std::string memo) {
		print("swap::cancelswap called");
		require_auth(user);

		auto trxs_iterator = _trxs.find(req_id);
		check(trxs_iterator != _trxs.end(), "Can not find request");
		check(trxs_iterator->from_user == user, "user info mismatch");
		check(trxs_iterator->pool_name == pool_name, "pool info mismatch");

		if (trxs_iterator->status == WAIT_COLLECT) {
			//remove trx_req
			_trxs.erase (trxs_iterator);

			//cancel payment request
			send_cnfmcs(req_id);

		} else {
			check (false, "#ERROR");
		}		
	}	

	ACTION swap::sync(uint64_t req_id,
					  bool     sync_result,
					  string   sync_meta,
					  string   memo) {
		print("swap::sync");
		require_auth(SWAP_ORACLE);

		auto trxs_iterator = _trxs.find(req_id);
		check (trxs_iterator != _trxs.end(), "invalid req_id");	

		auto pool_iterator = _pools.find(trxs_iterator->pool_name.value);
		check (pool_iterator != _pools.end(), "Can not find pool");
		check (pool_iterator->pool_status == POOL_OK, "pool status error");

		if (trxs_iterator->type == SWAP) {
			if (sync_result) {
				if (trxs_iterator->status == WAIT_COLLECT) {
					send_addauthreq(req_id, sync_meta);
					_trxs.modify(trxs_iterator, _self, [&](auto &upd){
						upd.status = WAIT_ORACLE;
						upd.sync_meta = sync_meta;
					});

					//remove collect request
					send_cnfmcs(req_id);

				}  else if (trxs_iterator->status == WAIT_SETTLE) {
					//send swap result to pool 
					//sent statement to swap.oracle					
					send_statmnt(trxs_iterator->trx_id,
							     trxs_iterator->from_user,
								 trxs_iterator->to_user,
								 SWAP,
								 trxs_iterator->pool_token,
								 trxs_iterator->from_token,
								 trxs_iterator->final_to_token,
								 trxs_iterator->fee,
								 string("swap successful"));
													 
					//remove trans request
					_trxs.erase(trxs_iterator);

					//remove settle request
					send_cnfmcs(req_id);
				} 
			} else {
				//sync failed, send statement to swap.oracle
				send_statmnt(trxs_iterator->trx_id,
								trxs_iterator->from_user,
								trxs_iterator->to_user,
								SWAP,
								trxs_iterator->pool_token,
								trxs_iterator->from_token,
								trxs_iterator->to_token,
								trxs_iterator->fee,
								string("swap failed (sync failed)")
							);

				//remove trx item
				_trxs.erase(trxs_iterator);							

				//remove request
				send_cnfmcs(req_id);
			}
		} else if (trxs_iterator->type == ADD_LIQUID) {			
			if (sync_result) {
				if (trxs_iterator->status == WAIT_COLLECT) {
					send_addauthreq(req_id, sync_meta);
					_trxs.modify(trxs_iterator, _self, [&](auto &upd){
						upd.status = WAIT_ORACLE;
						upd.sync_meta = sync_meta;
					});

					//remove collect request
					send_cnfmcs(req_id);
				} 
			} else {
				//sync failed, send statement to swap.oracle
				send_statmnt(trxs_iterator->trx_id,
								trxs_iterator->from_user,
								trxs_iterator->to_user,
								ADD_LIQUID,
								trxs_iterator->pool_token,
								trxs_iterator->from_token,
								trxs_iterator->to_token,
								trxs_iterator->fee,
								memo
							);

				//remove trx item
				_trxs.erase(trxs_iterator);							

				//remove request
				send_cnfmcs(req_id);
			}

		} else if (trxs_iterator->type == RM_LIQUID) {
			if (sync_result) {
				if (trxs_iterator->status == WAIT_SETTLE) {
					send_addauthreq(req_id, sync_meta);
					_trxs.modify(trxs_iterator, _self, [&](auto &upd){
						upd.status = WAIT_ORACLE;
						upd.sync_meta = sync_meta;
					});
					send_cnfmcs(req_id);
				}
			} else {
				//sync failed
				//refund user
				action(
					permission_level{_self, "active"_n},
					"eosio.token"_n,
					"transfer"_n,
					std::make_tuple(_self,
									trxs_iterator->from_user,
									trxs_iterator->pool_token,
									std::string("refund pool token"))
				).send();
				// send statement to swap.oracle
				send_statmnt(trxs_iterator->trx_id,
							 trxs_iterator->from_user,
							 trxs_iterator->to_user,
							 RM_LIQUID,
							 trxs_iterator->pool_token,
							 trxs_iterator->from_token,
							 trxs_iterator->to_token,
							 trxs_iterator->fee,
							 memo);											 
				
				_trxs.erase(trxs_iterator);
				send_cnfmcs(req_id);
			}					
		} else {
			check (false, "TYPE NOT SUPPORTED");
		}
	} 

	ACTION swap::updauth(uint64_t req_id,
					     bool     auth_result,
					     string   auth_meta,
					     string   memo) {
		print("swap::updauth");
		require_auth(SWAP_ORACLE);

		auto trxs_iterator = _trxs.find(req_id);
		check (trxs_iterator != _trxs.end(), "Can not find swap item");	

		auto pool_iterator = _pools.find(trxs_iterator->pool_name.value);
		check (pool_iterator != _pools.end(), "Can not find pool");
		check (pool_iterator->pool_status == POOL_OK, "pool status error");

		if (trxs_iterator->type == SWAP) {
			if (trxs_iterator->status == WAIT_ORACLE) {
				if (auth_result) {
					asset amount = trxs_iterator->from_token;
					float slippage = (float)(1.0f - (float)(trxs_iterator->slippage)/100.0f);
					asset orig_to_token_slippaged = asset( trxs_iterator->to_token.amount * slippage, trxs_iterator->to_token.symbol);			
					asset trx_fee = asset(amount.amount * (float) 0.003, amount.symbol);
					asset amount_after_fee = asset(amount.amount - trx_fee.amount, amount.symbol);

					asset t1 = pool_iterator->token1;
					asset t2 = pool_iterator->token2;		

					asset new_to_token_af; //new to_token after fee
					asset new_token1;
					asset new_token2;

					bool should_reject = false;
		
					if (amount.symbol == t1.symbol) {
						new_to_token_af = get_to_token(t1, t2, amount_after_fee);
						//BigUnsigned r = invariant/(bi1 - amaf) - bi2;
						//new_to_token_af = asset(r.toUnsignedLong(), t2.symbol);
						if (t2.amount < new_to_token_af.amount) {
							should_reject = true;
						} else {
							new_token1 = asset(t1.amount + amount.amount, t1.symbol);
							new_token2 = asset(t2.amount - new_to_token_af.amount, t2.symbol);
						}	
					} else {
						new_to_token_af = get_to_token(t2, t1, amount_after_fee);
						if (t1.amount < new_to_token_af.amount) {
							should_reject = true;
						} else {
							new_token1 = asset(t1.amount - new_to_token_af.amount, t1.symbol);
							new_token2 = asset(t2.amount + amount.amount, t2.symbol);
						}
					}						

					if (should_reject) {
						//overdraw, send money back to user
						_trxs.modify(trxs_iterator, _self, [&](auto &upd){
							upd.status = WAIT_REFUND;
							upd.fail_str = "token overdraw";
						});	

						//send_refund (settle) to swap.oracle
					
					} else if (new_to_token_af.amount < orig_to_token_slippaged.amount) {
						//beyond slippage, send money back to user
						_trxs.modify(trxs_iterator, _self, [&](auto &upd){
							upd.status = WAIT_REFUND;
							upd.fail_str = "beyond slippage";
						});		

						//send_refund (settle) to swap.oracle

					} else {
						//send settle request to swap.oracle
						send_addsettle(req_id, 
								   std::string(trxs_iterator->mixin_trace_id),
								   std::string(trxs_iterator->mixin_account_id),
								   trxs_iterator->to_user,
								   asset(0, pool_iterator->pool_token.symbol),
								   asset(0, amount.symbol),
								   new_to_token_af,
								   trxs_iterator->sync_meta,
								   std::string("pay token to user"));
					
						//update trxs
						_trxs.modify(trxs_iterator, _self, [&](auto &upd){
							upd.status = WAIT_SETTLE;
							upd.fee    = trx_fee;
							upd.final_to_token = new_to_token_af;
						});

						//update pool
						_pools.modify(pool_iterator, _self, [&](auto &upd){
							upd.token1 = new_token1;
							upd.token2 = new_token2;
						});
					}

					//remove collect auth request
					send_cnfmauth(req_id);					
				} else {
					check (false, "auth failed");
				}
			} else {
				check (false, "trxs status error, please check manually");
			} 			
		} else if (trxs_iterator->type == ADD_LIQUID) {
			if (trxs_iterator->status == WAIT_ORACLE) {
				if (auth_result) {
					//remove auth
					send_cnfmauth(req_id);

					//send pool_token to user;
					send_transfer(trxs_iterator->from_user, trxs_iterator->pool_token, string(""));

					//send statmnt;
					send_statmnt(trxs_iterator->trx_id,
								 trxs_iterator->from_user,
								 trxs_iterator->to_user,
								 ADD_LIQUID,
								 trxs_iterator->pool_token,
								 trxs_iterator->from_token,
								 trxs_iterator->to_token,
								 trxs_iterator->fee,
								 string("add liquid successful")
							);

					//update pool
					_pools.modify(pool_iterator, _self, [&](auto &upd) {
						upd.token1 += trxs_iterator->from_token;
						upd.token2 += trxs_iterator->to_token;
						upd.pool_token += trxs_iterator->pool_token;
					});
					
					//remove trxs
					_trxs.erase(trxs_iterator);					
				} else {
					//remove auth
					send_cnfmauth(req_id);

					//send statmnt;
					send_statmnt(trxs_iterator->trx_id,
								 trxs_iterator->from_user,
								 trxs_iterator->to_user,
								 ADD_LIQUID,
								 trxs_iterator->pool_token,
								 trxs_iterator->from_token,
								 trxs_iterator->to_token,
								 trxs_iterator->fee,
								 memo);

					//remove trxs
					_trxs.erase(trxs_iterator);	
				}
			} else {
				check (false, "trxs status error, please check manually");
			}
		} else if (trxs_iterator->type == RM_LIQUID) {
			if (trxs_iterator->status == WAIT_ORACLE) {
				if (auth_result) {
					//remove auth
					send_cnfmauth(req_id);

					//send statmnt;
					send_statmnt(trxs_iterator->trx_id,
								 trxs_iterator->from_user,
								 trxs_iterator->to_user,
								 RM_LIQUID,
								 trxs_iterator->pool_token,
								 trxs_iterator->from_token,
								 trxs_iterator->to_token,
								 trxs_iterator->fee,
								 string("rm liquid successful")
							);

					//update pool
					_pools.modify(pool_iterator, _self, [&](auto &upd) {
						upd.token1 -= trxs_iterator->from_token;
						upd.token2 -= trxs_iterator->to_token;
						upd.pool_token -= trxs_iterator->pool_token;
					});
					
					//remove trxs
					_trxs.erase(trxs_iterator);					
				} else {
					//remove auth 
					send_cnfmauth(req_id);

					//send statment			
					send_statmnt(trxs_iterator->trx_id,
								 trxs_iterator->from_user,
								 trxs_iterator->to_user,
								 RM_LIQUID,
								 trxs_iterator->pool_token,
								 trxs_iterator->from_token,
								 trxs_iterator->to_token,
								 trxs_iterator->fee,
								 memo);
					//remove trxs							
					_trxs.erase(trxs_iterator);								
				}
			} else {
				check (false, "trxs status error, please check manually");
			}			
		} else {
			check(false, "UNSUPPORTED OPERATION");
		}
	}

	ACTION swap::setpooltk (name  creator,
					  name  pool_name,
					  asset pool_token,
					  std::string memo) {
		require_auth(_self);
		auto pool_iterator = _pools.find(pool_name.value);
		check (pool_iterator != _pools.end(), "Can not find pool");
		check (pool_iterator->pool_status == POOL_OK, "pool status error");

		_pools.modify(pool_iterator, _self, [&](auto &upd) {
			upd.pool_token = pool_token;
		});
	}

	bool swap::send_cnfmauth(uint64_t req_id) {
		action(
			permission_level{_self, "active"_n},
			SWAP_ORACLE, 
			"cnfmauth"_n,
			std::make_tuple(req_id, 
			std::string("confirm auth request"))
		).send();

		return true;
	}
	
	bool swap::send_cnfmcs(uint64_t req_id) {
		action(
			permission_level{_self, "active"_n},
			SWAP_ORACLE, 
			"cnfmcs"_n,
			std::make_tuple(req_id, 
			std::string("confirm collect/setttle request"))
		).send();		
		return true;
	}

	bool swap::send_addauthreq(uint64_t req_id, string sync_meta) {
		//send auth request to swap.oracle		
		action(
			permission_level{_self, "active"_n},
			SWAP_ORACLE, 
			"addauthreq"_n,
			std::make_tuple(req_id,							 
							sync_meta,							
							std::string("auth request"))
		).send();

		return true;					
	}					

	bool swap::send_addcollect(uint64_t req_id,
							   string   mixin_trace_id,
							   string   mixin_account_id,
							   name     from_user,
							   asset    pool_token,
							   asset    token1, 
							   asset    token2,
							   string   sync_meta,   
							   string   memo) {
		action(
			permission_level{_self, "active"_n},
			SWAP_ORACLE, 
			"addcollect"_n,
			std::make_tuple(req_id, 
							mixin_trace_id, 
							mixin_account_id, 
							from_user, 
							pool_token,
							token1,
							token2,
							sync_meta,
							memo)
		).send();
		return true;
	}

	bool swap::send_addsettle(uint64_t req_id,
							  string   mixin_trace_id,
							  string   mixin_account_id,
							  name     to_user,
							  asset    pool_token,
							  asset    token1, 
							  asset    token2,
							  string   sync_meta,						  
							  string   memo) {
		action(
			permission_level{_self, "active"_n},
			SWAP_ORACLE, 
			"addsettle"_n,
			std::make_tuple(req_id, 
							mixin_trace_id, 
							mixin_account_id, 
							to_user, 
							pool_token,
							token1,
							token2,
							sync_meta,
							memo)
		).send();
		return true;
	}

	bool swap::send_statmnt(uint64_t    req_id,
							name        from_user,
							name        to_user,
							uint8_t     type,
							asset       pool_token,
							asset       token1, 
							asset       token2,
							asset       fee,
							std::string memo) {
		action(
			permission_level{_self, "active"_n},
			SWAP_ORACLE, 
			"addstatmnt"_n,
			std::make_tuple(req_id, 
							from_user, 
							to_user, 
							type, 
							pool_token,
							token1,
							token2,
							fee,
							memo)
		).send();
		return true;
	}

	bool swap::send_transfer(name to, 
							 asset amount,
						     string memo) {
		action(
			permission_level{_self, "active"_n},
			"eosio.token"_n,
			"transfer"_n,
			std::make_tuple(_self,
							to,
							amount,
							std::string(""))
		).send();
		return true;	
	}

	checksum256 swap::get_trx_id() {
    	size_t size = transaction_size();
    	char buf[size];
    	size_t read = read_transaction( buf, size );
    	check( size == read, "read_transaction failed");
    	return sha256( buf, read );
	}

	uint32_t swap::get_current_block_time() {
		eosio::time_point now = eosio::current_block_time(); 
        uint32_t time_stamp = now.sec_since_epoch();
		return time_stamp;
	}

	BigUnsigned swap::get_sqrt(asset t1, asset t2) {	
		string str_t1 = std::to_string(t1.amount);
		string str_t2 = std::to_string(t2.amount);
		BigUnsigned bg1 = stringToBigUnsigned(str_t1 + "0");
		BigUnsigned bg2 = stringToBigUnsigned(str_t2 + "0");

		return bsqrt(stringToBigUnsigned(bigUnsignedToString(bg1 * bg2) + "0"));
	}

	BigUnsigned swap::bsqrt(BigUnsigned n) {

		if (n == 0) {
			return BigUnsigned(0);
		} else if (n == 1) {
			return BigUnsigned(1);
		} else {
			BigUnsigned a(n);
			BigUnsigned b;
			BigUnsigned eps(1);			
			while(1)
			{
				b = a/2 + n/a/2;
				if (b + eps > a)
				{
					return b;
				}
				a = b;
			}
		}
	}

	asset swap::get_to_token(asset t1, asset t2, asset am) {

		/*
		string str_t1 = std::to_string(t1.amount);
		string str_t2 = std::to_string(t2.amount);
		string str_t3 = std::to_string(am.amount);

		BigUnsigned bt1 = stringToBigUnsigned(str_t1 + "0");
		BigUnsigned bt2 = stringToBigUnsigned(str_t2 + "0");
		BigUnsigned bam = stringToBigUnsigned(str_t3 + "0");
	
		BigUnsigned r = bt2 - bt1 * bt2 / (bt1 + bam);
		return asset(getll(r), t2.symbol);
		*/		
		double d1 = (double)t1.amount;
		double d2 = (double)t2.amount;		
		double dm = (double)am.amount;

		long double r = d2 - d1 * d2 / (d1 + dm);

		check(r >= 1, "less than the minimum convertible amount");

		return asset((long long)r, t2.symbol);
	} 

	long long swap::getll(BigUnsigned n) {
		std::string::size_type sz = 0; 
  		long long ll = std::stoll (bigIntegerToString(n), &sz,0);

		return ll;
	}
} //namespace
