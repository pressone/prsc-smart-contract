/**
 *  @file
 *  @copyright defined in LICENSE.txt
 */

#pragma once

/*
    contracts wide enum, error string or message string goes here
 */

namespace prs {

    const static eosio::name PRS_SWAP    = "prs.swap"_n;
    const static eosio::name SWAP_ORACLE = "swap.oracle"_n;
    const static eosio::name EOSIO_TOKEN = "eosio.token"_n;
    const static eosio::name SWAP_POOL   = "swap.pool"_n;
    
    enum RESULT_CODE {
        OK = 1, 
        WARNING = 2, 
        ERROR = 3, 
        FATAL_ERROR =4 
    };

    static const char * ERROR_POOL_EXIST           = "pool existed";
    static const char * ERROR_POOL_NOT_EXIST       = "pool not existed";
    static const char * ERROR_CREATOR_MISMATCH     = "only creator can remove the pool";
    static const char * ERROR_TOKEN_INVALID        = "token name mistach or invalid token amount";
    static const char * ERROR_INVALID_POOL_STATUS  = "pool status error";
    static const char * ERROR_USER_HAS_REQUEST     = "user already submit swap request";
    static const char * ERROR_TOKEN_RATIO_MISMATCH = "token ratios mismatch";
}


