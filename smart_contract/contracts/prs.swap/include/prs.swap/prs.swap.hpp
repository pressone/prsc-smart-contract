#pragma once

#include <eosio/eosio.hpp>
#include <eosio/asset.hpp>
#include <eosio/system.hpp>
#include <eosio/singleton.hpp>
#include <string>

#include "../lib/BigIntegerLibrary.hpp" 

using std::string;
using std::vector;
using eosio::name;
using eosio::datastream;
using eosio::asset;
using eosio::current_time_point;

namespace prs{

	using namespace eosio;
   
	enum TASK_TYPE {
		SWAP       = 1,
		ADD_LIQUID = 2,  // user add liquids to pool  
		RM_LIQUID  = 3   // user remove liquids from pool
	};

	enum POOL_STATUS {
		POOL_OK   = 1,
		POOL_GONE = 2
	};

	enum ITEM_STATUS {
		WAIT_COLLECT = 1, 
		WAIT_SETTLE  = 2,		
		WAIT_ORACLE  = 3,
		WAIT_REFUND  = 4, 
		SUCCESS      = 5, 
		FAILED       = 5,
	};
	
	const static float TOKEN_RATIO_THRESHOLD = 0.0001; // 0.01%
	class [[eosio::contract]] swap : public contract {
		
	  public:
		swap( name receiver, name code, datastream<const char*> ds )
			   :contract(receiver, code, ds), 
			    _pools(receiver, receiver.value), 
				_trxs(receiver, receiver.value){}
		~swap() {}

		//ACTIONS

		//***********************************************************************************
		//*	Below are all pool factory related API, 
		//* will be exec manual by using prs.swap privillage
	    //***********************************************************************************			

		/*
			Name: addpool
			Desc: add a new token pair pool
			      total amount of 2 tokens will be set to 0
				  a new pool token (EOS asset) should be created and issue to prs.swap before call addExchg
			      pool status will be set to CREATED
		*/
		ACTION addpool (name  creator,
						name  pool_name,
						asset token1,						
						asset token2,
						asset pool_token,
						std::string memo);

		/*
			Name: rmpool
			Desc: remove a pool (token pair)
		*/
		ACTION rmpool (name creator,
					   name pool_name,
					   std::string memo);

		/***********************************************************************************
		 * below are APIs related with prs-ATM, for details please check design document
	     **********************************************************************************/

		/*
			Name: swaptoken
			Desc: pay certain amount of token_1 and recieive certain amount of token_2 according 
			      to exchange rate (pool ratio), 0.3% of token_1 will be collected and put back to 
				  pool as reward for liquidity provider. 
		*/
		ACTION swaptoken (uint64_t    req_id,
						  name        from_user,
						  name        to_user,
				          name        pool_name,
				          asset       amount,
					      uint8_t     slippage,
					      std::string mixin_trace_id,
					      std::string mixin_account_id,
                          std::string memo);
		/*
			Name: addliquid
			Desc: liquidity provider save certain amount of token pair to add liquidity to the pool,
			      LP will receive pool token as an evidence for provide liquidity and collect reward
				  when return pool token back to a pool, no fees are collected
		*/
		ACTION addliquid (uint64_t    req_id,
						  name        user,
		                  name        pool_name, 
						  asset       token1_amount,
						  asset       token2_amount,
						  std::string mixin_trace_id,
						  std::string mixin_account_id,
						  std::string memo);
		/*
			Name: rmliquid
			Desc: liquidity provider payback pool token and get certain amount of pair token
			      after remove liquidity from a pool, certain amount of pool token will be "burn"
				  and 2 tokens will be send to LP's mixin wallet (according to current exchange ratio)
		*/	
		ACTION rmliquid (uint64_t    req_id,
						 name        user,
						 name        pool_name,
						 asset       pool_token_amount,
						 std::string mixin_trace_id,
						 std::string mixin_account_id,
						 std::string memo);
		/*
			Name: cancel
			Desc: cancel swap request (before make payment)
			      no fees are collected
		*/
		ACTION cancel (uint64_t    req_id,
					   name        user,
					   name        pool_name,			
					   std::string memo);						 

		/***********************************************************************************
		 * below are APIs related with PRS payment gateway and oracle service,
		 * for details please check design document
	    ***********************************************************************************/
		 
	    ACTION sync(uint64_t    req_id,
				    bool        sync_result,
				    std::string sync_meta,
					std::string memo);				

		ACTION updauth(uint64_t    req_id,
					   bool        auth_result,
					   std::string auth_meta,
					   std::string memo);

		//test only !!!
	    ACTION setpooltk (name  creator,
						  name  pool_name,
						  asset pool_token,
						  std::string memo);

		//"pool_token": "",					   
			
		//Action Wrappers
		using addpool_action   = action_wrapper<"addpool"_n,    &swap::addpool>;
		using rmpool_action    = action_wrapper<"rmpool"_n,     &swap::rmpool>;
		using swaptoken_action = action_wrapper<"swaptoken"_n,  &swap::swaptoken>;	
		using addliquid_action = action_wrapper<"addliquid"_n,  &swap::addliquid>;		
		using rmliquid_action  = action_wrapper<"rmliquid"_n,   &swap::rmliquid>;
		using cancel_action    = action_wrapper<"cancel"_n,     &swap::cancel>;						
		using sync_action      = action_wrapper<"sync"_n,       &swap::sync>;
		using updauth_action   = action_wrapper<"updauth"_n,    &swap::updauth>;
		using setpooltk_action = action_wrapper<"setpooltk"_n,  &swap::setpooltk>;
						
	  private:
	  	struct [[eosio::table]] transitem {
            uint64_t    trx_id;			  	
			name        from_user;
			name        to_user;
			uint8_t     type;
			uint8_t     status;
			name        pool_name;
			asset       pool_token;
			asset       from_token;
			asset       to_token;
			asset       final_to_token;
			asset       fee;
			uint8_t     slippage = 0;
			uint32_t    req_timestamp = 0;
			checksum256 req_trx_id;	
			std::string oracle_info;
			checksum256 oracle_trx_id;
			uint32_t    oracle_timestamp = 0;
			std::string mixin_trace_id = "";
			std::string mixin_account_id = "";
			std::string sync_meta = "";
			std::string fail_str = "";

			auto primary_key() const { return trx_id; }
			uint64_t by_user_account() const { return from_user.value; }			
		};

		struct  [[eosio::table]] poolitem {
			name  pool_name;
			name  pool_creator;
			uint8_t pool_status;
			asset token1;
			asset token2;
			asset pool_token;
			uint32_t created_timestamp;

			auto primary_key() const { return pool_name.value; }
		};

		using pool_index        = eosio::multi_index<"pool"_n, poolitem>;
		using transitem_index   = eosio::multi_index<"trxs"_n, transitem,
		indexed_by<"byuser"_n, const_mem_fun<transitem, uint64_t, &transitem::by_user_account>>>;

		pool_index        _pools;
		transitem_index   _trxs;

		bool send_cnfmauth(uint64_t req_id);
		bool send_cnfmcs(uint64_t req_id);
		bool send_addcollect(uint64_t    req_id,
							 std::string mixin_trace_id,
							 std::string mixin_account_id,
							 name        from_user,
							 asset       pool_token,
							 asset       token1, 
							 asset       token2,
							 std::string sync_meta,
							 std::string memo);
		bool send_addsettle(uint64_t    req_id,
							std::string mixin_trace_id,
							std::string mixin_account_id,
							name        to_user,
							asset       pool_token,
							asset       token1, 
							asset       token2,
							std::string	sync_meta,
							std::string memo);
		bool send_addauthreq (uint64_t reqid, std::string sync_meta);
		bool send_statmnt(uint64_t    req_id,
			  			  name        from_user,
						  name        to_user,
						  uint8_t     type,
						  asset       pool_token,
						  asset       token1, 
						  asset       token2,
						  asset       fee,
						  std::string memo);
		bool send_transfer(name to, 
						   asset amount,
						   std::string memo);									   					   

		checksum256 get_trx_id();
		uint32_t    get_current_block_time();
		BigUnsigned get_sqrt(asset t1, asset t2);
		BigUnsigned bsqrt(BigUnsigned a);
		asset       get_to_token(asset t1, asset t2, asset am);
		long long   getll(BigUnsigned n);
	};
}