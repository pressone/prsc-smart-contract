#pragma once

#include <eosio/eosio.hpp>
#include <string>

using std::string;
using eosio::name;
using eosio::datastream;

namespace prs{

	using namespace eosio;

	enum RESULT_CODE {
		OK = 			1, 
		WARNING = 		2, 
		ERROR = 		3,
		FATAL_ERROR =	4 
	};
	
	class [[eosio::contract]] oracle : public contract {
		
	  public:
		oracle( name receiver, name code, datastream<const char*> ds )
			   :contract(receiver, code, ds) {}

		//Actions		
		ACTION addauthreq(name sender,		            
                          std::string auth_content);
		
	    ACTION updresult(name        oracleservice,
		                 uint64_t    auth_hash,					    
		                 std::string type,
						 std::string meta,
						 std::string data,
						 std::string memo);

		ACTION purgeall(name oracleservice, std::string memo);

		//Action Wrappers
		using addauthreq_action = action_wrapper<"addauthreq"_n, &oracle::addauthreq>;
		using updresult_action  = action_wrapper<"updresult"_n, &oracle::updresult>;	
		using purgeall_action   = action_wrapper<"purgeall"_n, &oracle::purgeall>;
		
	  private:

        struct  [[eosio::table]]  authitems {
            uint64_t          id = 0;
			std::string       auth_content = "";			
			uint32_t          time_stamp_req = 0;
			checksum256       trx_id;

            uint64_t primary_key() const { return id; }
        };

        typedef multi_index<"auth"_n, authitems> auth_index;

		checksum256 get_trx_id();
	};
}