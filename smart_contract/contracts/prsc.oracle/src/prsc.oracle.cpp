#include <prsc.oracle/prsc.oracle.hpp>
#include <eosio/system.hpp>
#include <eosio/transaction.hpp>
#include <eosio/crypto.hpp>

namespace prs{

	using std::string;
	using namespace eosio;

	ACTION oracle::addauthreq(name   sender,            
                              string auth_content) {	
		print("oracle::addauthreq called");
		require_auth("prs.prsc"_n);

        eosio::time_point now = eosio::current_block_time(); 
        uint32_t time_stamp = now.sec_since_epoch();

 		auth_index auth(get_self(), get_first_receiver().value);
	    uint64_t primary_key = auth.available_primary_key();
		auth.emplace(_self, [&](auto& obj) {
				obj.id = primary_key;
				obj.auth_content = auth_content;
				obj.time_stamp_req = time_stamp;
				obj.trx_id = get_trx_id();
		});
	}
	
	ACTION oracle::updresult(name     oracleservice, 
	                         uint64_t auth_hash,					    
		                 	 string type,
							 string meta,
						     string data,
							 string memo) {
		print("oracle::updresult called");
		require_auth(oracleservice);

        eosio::time_point now = eosio::current_block_time(); 
        uint32_t time_stamp = now.sec_since_epoch();

 		auth_index auth(get_self(), get_first_receiver().value);
	
		auto iter = auth.find(auth_hash);
		if (iter == auth.end()) {
			check(false, "auth_hash is invalid");
		} else {
			auth.erase(iter);			
		}				
	}

	ACTION oracle::purgeall(name oracleservice, string memo) {
		print("oracle::purgeall called");
		require_auth(oracleservice);

		// purge all contents
	 	auth_index auth(get_self(), get_first_receiver().value);        
		for(auto itr = auth.begin(); itr != auth.end();) {
            itr = auth.erase(itr);        
        }		 	
	}

	checksum256 oracle::get_trx_id() {
    	size_t size = transaction_size();
    	char buf[size];
    	size_t read = read_transaction( buf, size );
    	check( size == read, "read_transaction failed");
    	return sha256( buf, read );
	}
} //namespace
