add_executable( swap.oracle ${CMAKE_CURRENT_SOURCE_DIR}/src/swap.oracle.cpp )
target_compile_options( swap.oracle PUBLIC -abigen )
get_target_property(BINOUTPUT swap.oracle BINARY_DIR)
target_compile_options( swap.oracle PUBLIC -abigen_output=${BINOUTPUT}/${TARGET}.abi )
target_compile_options( swap.oracle PUBLIC -contract soracle )

target_include_directories(swap.oracle
   PUBLIC
   ${CMAKE_CURRENT_SOURCE_DIR}/include)

set_target_properties(swap.oracle
   PROPERTIES
   RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
