#pragma once

#include <eosio/eosio.hpp>
#include <eosio/asset.hpp>
#include <string>

using std::string;
using eosio::name;
using eosio::datastream;
using eosio::asset;

namespace prs{

	using namespace eosio;
	
	enum TASK_TYPE {
		COLLECT    = 1,  // collect tokens from user
		SETTLE     = 2   // send tokens to user
	};

	enum STATUS {
		REQ_RECEIVED = 1, 
		PAID     = 2,
		RETURNED = 3
	};

	class [[eosio::contract]] soracle : public contract {
		
	  public:
		soracle( name receiver, name code, datastream<const char*> ds )
			   :contract(receiver, code, ds),
			    _auth (receiver, receiver.value),
			    _trans (receiver, receiver.value),
		        _statmnt (receiver, receiver.value) {}
		~soracle() {}


		//Actions		
		ACTION addauthreq(uint64_t    req_id,
                          std::string auth_content,				  
						  std::string memo);

	    ACTION cnfmauth(uint64_t    req_id,
						std::string memo);						  

		ACTION addcollect(uint64_t    req_id,
		                  std::string mixin_trace_id,
					      std::string mixin_account_id,
					      name        user,
						  asset       pool_token,
		     		      asset       token1,
					      asset       token2,
 						  std::string sync_meta,
					      std::string memo);

		ACTION addsettle(uint64_t    req_id,
						 std::string mixin_trace_id,
						 std::string mixin_account_id,
						 name        user, 
						 asset       pool_token,
						 asset       token1,
						 asset       token2,
						 std::string sync_meta,
						 std::string memo);	

		ACTION cnfmcs (uint64_t req_id,
					   std::string memo);
								
		ACTION addstatmnt(uint64_t   req_id,			
					      name       from_user,
			              name       to_user,
					      uint8_t    type,
						  asset      pool_token,
						  asset      token1,
						  asset      token2,
						  asset      fee,														
						  std::string memo);

		ACTION cnfmstatmnt(uint64_t id,
						   std::string memo);

		// clear all statment, dev only
		ACTION clrstatmnt(std::string memo);
	
		//Action Wrappers
		using addauthreq_action  = action_wrapper<"addauthreq"_n,  &soracle::addauthreq>;
		using cnfmauth_action    = action_wrapper<"cnfmauth"_n,    &soracle::cnfmauth>;
		using addcollect_action  = action_wrapper<"addcollect"_n,  &soracle::addcollect>;
		using addsettle_action   = action_wrapper<"addsettle"_n,   &soracle::addsettle>;
		using cnfmcs_action      = action_wrapper<"cnfmcs"_n,      &soracle::cnfmcs>;		
		using addstatmnt_action  = action_wrapper<"addstatmnt"_n,  &soracle::addstatmnt>;
		using cnfmstatmnt_action = action_wrapper<"cnfmstatmnt"_n, &soracle::cnfmstatmnt>;
		using clrstatmnt_action  = action_wrapper<"clrstatmnt"_n,  &soracle::clrstatmnt>;

	  private:

        struct  [[eosio::table]]  authitems {
            uint64_t          id = 0;
			std::string       auth_content = "";			
			uint32_t          timestamp_req = 0;
			checksum256       trx_id;

            uint64_t primary_key() const { return id; }
        };

		struct [[eosio::table]] transitems {
			uint64_t    id = 0;
			std::string mixin_trace_id = "";
			std::string mixin_account_id = "";
			name        user;
			uint8_t     type;
			asset       pool_token;
			asset       token1;
			asset       token2;	
			std::string sync_meta = "";
			uint32_t    timestamp_received = 0;			
			uint32_t    timestamp_processed = 0;
			checksum256 trx_id_received;
			checksum256 trx_id_processed;
			uint8_t     status;

            uint64_t primary_key() const { return id; }			
		};

		struct [[eosio::table]] statmntitems {
			uint64_t    id = 0;
			name        from_user;
			name        to_user;
			uint8_t     type;
			asset       pool_token;
			asset       token1;
			asset       token2;
			asset       fee;
			uint32_t    timestamp;
			checksum256 trx_id;
			std::string memo;

			uint64_t primary_key() const { return id; }
		};

        typedef eosio::multi_index<"auth"_n, authitems> auth_table;
		typedef eosio::multi_index<"trans"_n, transitems> trans_table;
		typedef eosio::multi_index<"statmnt"_n, statmntitems> statmnt_table;

		auth_table    _auth;
		trans_table   _trans;
		statmnt_table _statmnt;

		checksum256 get_trx_id();
		uint32_t get_current_block_time();		
	}; //class
}