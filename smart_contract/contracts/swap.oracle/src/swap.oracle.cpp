#include <swap.oracle/swap.oracle.hpp>
#include <eosio/system.hpp>
#include <eosio/transaction.hpp>
#include <eosio/crypto.hpp>

namespace prs{

	using std::string;
	using namespace eosio;

	ACTION soracle::addauthreq(uint64_t req_id,
                               string   auth_content,				  
						       string   memo) {
		print("soracle::addauthreq called");
		require_auth("prs.swap"_n);

		//add auth req to auth table
		_auth.emplace(_self, [&](auto& new_auth){
			new_auth.id = req_id;
			new_auth.auth_content = auth_content;
			new_auth.timestamp_req = get_current_block_time();
			new_auth.trx_id = get_trx_id();
		});
	}

	ACTION soracle::cnfmauth(uint64_t    req_id,
						     std::string memo) {
		print("oracle::cnfmauth called");
		require_auth("prs.swap"_n);

		//remove auth req from auth table
		auto auth_iterator = _auth.find(req_id);
		check (auth_iterator != _auth.end(), "Can not find auth req");
		_auth.erase(auth_iterator);			
	}

	ACTION soracle::addcollect(uint64_t req_id,
		                       string   mixin_trace_id,
					           string   mixin_account_id,
					           name     user,
							   asset    pool_token,
		     		           asset    token1,
					           asset    token2,
							   string   sync_meta,
					           string   memo) {
		print("soracle::addcollect called");
		require_auth("prs.swap"_n);

		_trans.emplace(_self, [&](auto& new_trans){
			new_trans.id = req_id,
			new_trans.mixin_trace_id = mixin_trace_id;
			new_trans.mixin_account_id = mixin_account_id;
			new_trans.user = user;
			new_trans.type = COLLECT;
			new_trans.pool_token = pool_token;
			new_trans.token1 = token1;
			new_trans.token2 = token2;
			new_trans.sync_meta = sync_meta;
			new_trans.timestamp_received = get_current_block_time();
			new_trans.trx_id_received = get_trx_id();
			new_trans.status = REQ_RECEIVED;		
		});		
	}

	ACTION soracle::addsettle(uint64_t req_id,
						      string   mixin_trace_id,
						      string   mixin_account_id,
						      name     user, 
							  asset    pool_token,
						      asset    token1,
						      asset    token2,
 							  string   sync_meta,
						      string   memo) {
		print("soracle::addsettle called");
		require_auth("prs.swap"_n);	

		_trans.emplace(_self, [&](auto& new_trans){
			new_trans.id = req_id,
			new_trans.mixin_trace_id = mixin_trace_id;
			new_trans.mixin_account_id = mixin_account_id;
			new_trans.user = user;
			new_trans.type = SETTLE;
			new_trans.pool_token = pool_token;
			new_trans.token1 = token1;
			new_trans.token2 = token2;
			new_trans.sync_meta = sync_meta;
			new_trans.timestamp_received = get_current_block_time();
			new_trans.trx_id_received = get_trx_id();
			new_trans.status = REQ_RECEIVED;		
		});				
	}
	
	ACTION soracle::cnfmcs (uint64_t req_id,
					        std::string memo) {
		print("soracle::cnfmcs called");
		require_auth("prs.swap"_n);

		auto trx_iterator = _trans.find(req_id);
		check (trx_iterator != _trans.end(), "Can not find trx");
		_trans.erase(trx_iterator);
	}

	ACTION soracle::addstatmnt(uint64_t req_id,
					           name     from_user,
			                   name     to_user,
					           uint8_t  type,
						       asset    pool_token,
						       asset    token1,
						       asset    token2,	
							   asset    fee,													
						       string   memo) {
		print("soracle::updresult called");
		require_auth("prs.swap"_n);

		_statmnt.emplace(_self, [&](auto& new_stat){
			new_stat.id = req_id,
			new_stat.from_user = from_user;
			new_stat.to_user = to_user;			
			new_stat.type = type;
			new_stat.pool_token = pool_token;
			new_stat.token1 = token1;
			new_stat.token2 = token2;
			new_stat.fee = fee;
			new_stat.timestamp = get_current_block_time();
			new_stat.trx_id = get_trx_id();	
			new_stat.memo   = memo;
		});
	}

	ACTION  soracle::cnfmstatmnt(uint64_t id,
						         std::string memo) {
        print("soracle::cnfmstatmnt called");
	    require_auth(_self);

		auto iterator = _statmnt.find(id);
		check (iterator != _statmnt.end(), "Can not find trx");
		_statmnt.erase(iterator);				
	}

	ACTION soracle::clrstatmnt(std::string memo) {
	    print("soracle::clrstatmnt called");
	    require_auth(_self);	
		for(auto itr = _statmnt.begin(); itr != _statmnt.end();) {
            itr = _statmnt.erase(itr);        
		}	
	}

	checksum256 soracle::get_trx_id() {
    	size_t size = transaction_size();
    	char buf[size];
    	size_t read = read_transaction( buf, size );
    	check( size == read, "read_transaction failed");
    	return sha256( buf, read );
	}

	uint32_t soracle::get_current_block_time() {
		eosio::time_point now = eosio::current_block_time(); 
        uint32_t time_stamp = now.sec_since_epoch();
		return time_stamp;
	}	
} //namespace
