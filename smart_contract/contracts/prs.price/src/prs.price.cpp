#include <prs.price/prs.price.hpp>
#include <eosio/system.hpp>
#include <eosio/transaction.hpp>
#include <eosio/crypto.hpp>
#include <math.h>

namespace prs{

	using std::string;
	using namespace eosio;

	ACTION price::update(name     user, 
	                     string   code,
					     uint64_t price,  
					     uint8_t  accuracy,
		    	         string   provider,  
					     uint64_t timestamp,
						 string   memo) {
		print("price::update called");
		require_auth (user);

		check(code.length() != 0, "invalid code");
		check(price > 0, "invlid price");
		check(accuracy > 0, "invlid accuracy");
		check(provider.length() != 0, "invalid provider");

		name tokenname;
		if (code == usdtusd) {
			tokenname = USDTUSDN;
		} else if (code == eosusd) {
			tokenname = EOSUSDN;
		} else if (code == btcusd) {
			tokenname = BTCUSDN;
		} else if (code == ethusd) {
			tokenname = ETHUSDN;
		} else if (code == prsusd) {
			tokenname = PRSUSDN;
		} else {
			check (false, "unsupported token exchange rate");
		}		

		uint64_t current_time = get_current_block_time(); 

		auto idx = _exchange.get_index<"bytoken"_n>();
		auto itr = idx.find(tokenname.value);
		
		if (itr != idx.end()) {
			// update

			print("find and update");

			vector<price_item> prices = itr->prices;

			uint64_t id = itr->id;
			auto primary_iter = _exchange.find(id);

			//push new item to the end of vector
			prices.push_back({
				user,
				price,
				accuracy,
				provider,
				timestamp,
				current_time,
				get_trx_id(),
				memo
			});	

			bool should_recal = false;

			if (prices.size() == (PRICE_ITEM_NUM + 1)) {				
				prices.erase(prices.begin());
				should_recal = true;
			}

			_exchange.modify(primary_iter, _self, [&](auto& obj){
				obj.prices   = prices;
			});				

			if (should_recal) {
				
				// update price
				long double new_price = 0;
				uint8_t max_accuracy = 0;

				for (auto price_iter = prices.begin(); price_iter != prices.end(); ++price_iter) {
					if (price_iter->accuracy > max_accuracy) {
						max_accuracy = price_iter->accuracy;
					}
				}

				if (max_accuracy > 0) {
					for (auto price_iter = prices.begin(); price_iter != prices.end(); ++price_iter) {
						if (price_iter->accuracy == max_accuracy) {
							new_price += price_iter->price;
						} else {
							new_price += price_iter->price * pow(10, max_accuracy - price_iter->accuracy);
						}
					}
				} else {
					check(false, "all accuracy equals to 0, check table for detail");
				}

				new_price = floorl( new_price / 5 );

				_exchange.modify(primary_iter, _self, [&](auto& obj){
					obj.price    = new_price;
					obj.accuracy = max_accuracy;				
				});
			}
		} else {
			print("creat and insert");
			// create and push back new price item
			vector<price_item> prices;
			prices.push_back({
				user,
				price,
				accuracy,
				provider,
				timestamp,
				current_time,
				get_trx_id(),
				memo
			});

			// insert new token
			_exchange.emplace(_self, [&](auto& obj) {
				obj.id       = _exchange.available_primary_key();
				obj.token    = tokenname;
				obj.price    = 0;      // invalid price
				obj.accuracy = 0;      // invalid accuracy
				obj.prices   = prices;
			});
		}
	}	

	ACTION price::clear() {	
		print("price::del called");
		require_auth(_self);

		for (auto itr = _exchange.begin(); itr != _exchange.end();) {
			itr = _exchange.erase(itr);
		}

	}

	uint64_t price::get_current_block_time() {
		eosio::time_point now = eosio::current_time_point(); 
        eosio::microseconds micros = now.time_since_epoch();
		uint64_t count = micros.count();
		return count;
	}

	checksum256 price::get_trx_id() {
    	size_t size = transaction_size();
    	char buf[size];
    	size_t read = read_transaction( buf, size );
    	check( size == read, "read_transaction failed");
    	return sha256( buf, read );
	}	

} //namespace

