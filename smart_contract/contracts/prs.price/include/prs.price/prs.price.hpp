#pragma once

#include <eosio/eosio.hpp>
#include <eosio/system.hpp>
#include <string>

using std::string;
using std::vector;
using eosio::name;
using eosio::datastream;
using eosio::current_time_point;

namespace prs{

	using namespace eosio;

	static constexpr uint32_t ms_per_5_minutes   = 300 * 1000;   
	static constexpr uint32_t seconds_per_hour   = 3600;
    static constexpr int64_t  seconds_per_day    = int64_t(seconds_per_hour) * 24;

	const string usdtusd = "USDT/USD";
	const string eosusd  = "EOS/USDT";
	const string ethusd  = "ETH/USDT";
	const string btcusd  = "BTC/USDT";
	const string prsusd  = "PRS/USDT";

	name USDTUSDN = name("usdt.usd");
	name EOSUSDN  = name("eos.usdt");
	name BTCUSDN  = name("btc.usdt");
	name ETHUSDN  = name("eth.usdt");
	name PRSUSDN  = name("prs.usdt");

	static constexpr uint8_t PRICE_ITEM_NUM = 5; // keep 5 latest price items

	class [[eosio::contract]] price : public contract {
		
	  public:
		price( name receiver, name code, datastream<const char*> ds )
			 :contract(receiver, code, ds),
			 _update(receiver, receiver.value),
			 _exchange(receiver, receiver.value) {};

	    ACTION update(name     user, 
	                  string   code,
					  uint64_t price,  
					  uint8_t  accuracy,
		    	      string   provider,  
					  uint64_t timestamp,
					  string   memo);

		ACTION clear();					  

		using update_action  = action_wrapper<"update"_n,  &price::update>;
		using clear_action   = action_wrapper<"clear"_n,   &price::clear>;

		private:

			struct price_item {
				name        user;
				uint64_t    price;
				uint8_t     accuracy;
				string      provider;
				uint64_t    user_timestamp;
				uint64_t    chain_timestamp;
				checksum256 trx_id;
				string      memo;
			};

			struct [[eosio::table]] exchange_item {
				uint64_t           id;
				name               token;
				uint64_t           price;
				uint8_t            accuracy;	
				vector<price_item> prices;

				auto      primary_key() const { return id; }
				uint64_t  by_token() const { return token.value; }
			};

			struct [[eosio::table]] update_item {
				uint64_t    id = 0;
				name        user;
				std::string code = "";
				uint64_t    price = 0;
				uint8_t     accuracy = 0; 
				string      provider = "";
				uint64_t    timestamp_user = 0;
				uint64_t    timestamp_chain = 0;
				checksum256 trx_id;	
				std::string memo = "";

				uint64_t    primary_key() const { return id; };
			};
			
			using update_index   = eosio::multi_index<"update"_n, update_item>;
			using exchange_index = eosio::multi_index<"exchange"_n, exchange_item,
				indexed_by<"bytoken"_n, const_mem_fun<exchange_item, uint64_t, &exchange_item::by_token>>>;			

			update_index    _update;
			exchange_index  _exchange;

			checksum256 get_trx_id();
			uint64_t get_current_block_time();
			
	}; //class
}