#! /bin/bash

printf "\t=========== Building prs.contracts ===========\n\n"

RED='\033[0;31m'
NC='\033[0m'

rm -rf build
CORES=`getconf _NPROCESSORS_ONLN`
mkdir -p build
pushd build &> /dev/null
cmake ../
make -j${CORES}
popd &> /dev/null
