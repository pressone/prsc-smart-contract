# prsc smart contract

## prs token deposit/withdraw procedure

open https://docs.google.com/document/d/1K_lvsLPxt1YRcpz2Tb-sWJ06XHv8DccUEUZdIL7ukz4 "graph2" for reference

### deposit
1. PRS Token holder (as "User") call PRS Token API with deposit request
2. PRS Token API generate a deposit_id (like "123456")
3. PRS Token API update deposit request to prs.tproxy contract
    ```
    example: testuser1 request deposit 1.0000 PRS
    
    cleos --url http://51.255.133.170:8888 push action prs.tproxy reqdeposit '["testuser1", 123456, "mixin_trace_id", "mixin_account_id", "1.0000 SRP", "memo"]' -p testuser1@active
    ```

4. prs.tproxy contract save the request to multi_index_table "req"

    ```
    cleos --url http://51.255.133.170:8888 get table prs.tproxy prs.tproxy req
    output:
    {
    "rows": [{
        "req_id": 1610495557,
        "mixin_trace_id": "mixin_trace_id",
        "mixin_account_id": "mixin_account_id",
        "user": "testuser1",
        "type": 1,
        "status": 1,
        "amount": "1.0000 SRP",
        "memo": "memo",
        "sync_content": "",
        "auth_content": "",
        "auth_result": 0,
        "time_stamp_req": 1610495557,
        "time_stamp_sync": 0,
        "time_stamp_auth": 0,
        "trx_id_req": "d6675332cae6d30f07e8b096de30a8aa4dcfedfa2d3d508715644a4388bb4e1d",
        "trx_id_sync": "0000000000000000000000000000000000000000000000000000000000000000",
        "trx_id_auth": "0000000000000000000000000000000000000000000000000000000000000000"
        }
    ],
    "more": false
    }
    ```

    - req_id should be unique
    - type:
		DEPOSIT  = 1
		WITHDRAW = 2
	- status:
		REQUESTED   = 1 
		WAIT_ORACLE = 2
		SUCCESS     = 3
		FAILED      = 4

5. User transfer certain amount PRS Token to deposit_id (Mixin)
6. Mixin Sync Service "sync" mixin transaction to prs.tproxy contract
   ```
   example: 
    cleos --url http://51.255.133.170:8888 push action prs.tproxy sync '["123456", true,  "{deposit_sync_meta}", "memo"]' -p prs.tproxy@active

    parameters:
    - "123456"            : deposit_id
    - true                : mixin token tranfer result 
    - {deposit_sync_meta} : mixin sync content
    - "memo"              " memo string

    check multi_index_table "req" (prs.tproxy)
    cleos --url http://51.255.133.170:8888 get table prs.tproxy prs.tproxy req
    {
    "rows": [{
        "req_id": 1610496429,
        "mixin_trace_id": "mixin_trace_id",
        "mixin_account_id": "mixin_account_id",
        "user": "testuser1",
        "type": 1,
        "status": 2,
        "amount": "1.0000 SRP",
        "memo": "memo",
        "sync_content": "{deposit_sync_meta}",
        "auth_content": "",
        "auth_result": 0,
        "time_stamp_req": 1610496429,
        "time_stamp_sync": 1610496506,
        "time_stamp_auth": 0,
        "trx_id_req": "a888eb498061d10688e48a14075352990c26544e1e7305dca3c475a0d04f1f2b",
        "trx_id_sync": "1e28b90ac0dc48558fe8d720fb96fc247077361701f5ab33d2d59638fa710db6",
        "trx_id_auth": "0000000000000000000000000000000000000000000000000000000000000000"
        }
    ],
    "more": false
    }

    after receive sync event from Mixin, prs.tproxy will send auth request to proxy.oracle,
    check multi_index_table "auth" (proxy.oracle)

    example:
    cleos --url http://51.255.133.170:8888 get table proxy.oracle proxy.oracle auth

    {
    "rows": [{
        "id": 1610496429,
        "mixin_trace_id": "mixin_trace_id",
        "auth_content": "{deposit_sync_meta}",
        "timestamp_req": 1610496506,
        "trx_id": "1e28b90ac0dc48558fe8d720fb96fc247077361701f5ab33d2d59638fa710db6"
        }
    ],
    "more": false
    }
    ```
    
    - req_id should match with recorder ("1610496429")
    - {deposit_sync_meta} will be send to proxy.oracle contract for auth
    - request status change to "WAIT_ORACLE" (2)
    - at this time point, certain amount of token will transfer from "pro.tproxy" to "proxy.oracle" (e.g 1 EOS)
    - proxy.oracle contract will save auth request and transfer recorder

7. Token oracle service should monitor "auth" table, get new items and send it to prsc service for authenticate
8. Token Oracle service update auth result to proxy.oracle contract

   ```
   example:
   cleos --url http://178.33.98.152:8888 push action proxy.oracle updresult '["123456", 1,  "auth_data", true, "memo"]' -p proxy.oracle@active 
   ```

   - ABI parameters: [req_id, type, auth_data, auth_result, memo]
    -- req_id should match recorder
    -- type should DEPOSIT (1)
    -- auth_data : any data string back from prsc service
    -- auth_result : true|false
    -- memo

   - if auth result is true, proxy.oracle will:
    -- update request status in prs.tproxy contract, change status to SUCCESS(3)
    -- transfer certain amount of token to user account
    -- delete auth recorder 
    -- update transfer recorder


   - final deposit request looks like:
   ``` 
    cleos --url http://178.33.98.152:8888 get table prs.tproxy prs.tproxy req

    {
    "rows": [{
        "req_id": 123456,
        "type": 1,
        "status": 3,
        "user": "testbp2",
        "amount": "1.0000 EOS",
        "sync_content": "{deposit_sync_meta}",
        "auth_content": "auth_data",
        "auth_result": 1,
        "time_stamp_req": 1571946809,
        "time_stamp_sync": 1571947299,
        "time_stamp_auth": 1571947888,
        "trx_id_req": "d78edb5e7539f7a65430b823c36e544e5ec799da51a71a7d7253871b4ade611b",
        "trx_id_sync": "14e353daf1e998c10a4aafdcf1abdb2488cfe3fd3594f631fa36e2e892df221b",
        "trx_id_auth": "b01f14d82a7deabce4f50d79e3cd489f481d105891d162ea93cb991b4db42843"
        }
    ],
    "more": false
    }    

    after proxy.oracle send auth result back, successful tranaction request will be deleted, failed transaction will be save till manaully process. 

    ```

    - transfer recorder in proxy.oracle contract looks like:

    ``` 
    cleos get table proxy.oracle proxy.oracle deposit

    {
    "rows": [{
        "id": 123456,
        "user": "testbp2",
        "amount": "1.0000 EOS",
        "timestamp_received": 1571947299,
        "timestamp_processed": 1571947888,
        "trx_id_received": "14e353daf1e998c10a4aafdcf1abdb2488cfe3fd3594f631fa36e2e892df221b",
        "trx_id_processed": "b01f14d82a7deabce4f50d79e3cd489f481d105891d162ea93cb991b4db42843",
        "status": 2
        }
    ],
    "more": false
    }
    ```   

### withdraw
1. to withdraw money, user should grant "eosio.code" permission to prs.tproxy contract, the following transction should be executed:

    ``` 
	cleos set account permission <USER_ACCOUNT> active <json file or string> owner -p <USER_ACCOUNT>

	example:

    testbp2 grant prs.tpxoy eosio.code permission:

    cleos --url http://178.33.98.152:8888 set account permission testbp2 active ./grant_auth.json owner -p testbp2

    grant_auth.json should follow the following format:
    
    {
    "threshold": 1,
        "keys": [
        {
            "key": "EOS7ZtBTNsekNJWJHMieEeELQWPpVwbKs7ezVE9nKk9rpkJ69wxUR", (user public key)
            "weight": 1
        }
        ],
        "accounts": [
        {
            "permission": 
            {
                "actor": "prs.tproxy",
                "permission": "eosio.code"
            },
            "weight": 1
        }
        ]
    }
    ```

2. User request withdraw from PRS Token API
3. PRS Token API generate withdraw_id
4. PRS Token API update withdraw request to prs.tproxy contract
5. if user has sufficient token, update will get a successful result
6. certail amount token will be transfer from user account to prs.tproxy account
7. a withdraw request will be add to "req" table
    ```
    example:    
        testbp2 request withdraw 1.0000 EOS

    cleos --url http://178.33.98.152:8888 push action prs.tproxy reqwithdraw '["testbp2", "123456", "1.0000 EOS", "memo"]' -p testbp2@active
    ``` 
    check "req" table, notice "type" is set to 2 (WITHDRAW)

    ```
    cleos --url http://178.33.98.152:8888 get table prs.tproxy prs.tproxy req
    {
    "rows": [{
        "req_id": 123456,
        "type": 2,
        "status": 1,
        "user": "testbp2",
        "amount": "1.0000 EOS",
        "sync_content": "",
        "auth_content": "",
        "auth_result": 0,
        "time_stamp_req": 1571949304,
        "time_stamp_sync": 0,
        "time_stamp_auth": 0,
        "trx_id_req": "9650e3927e0f433bb05934c98d570a2c984e2d2888ba1e1a3abaddff6f102f22",
        "trx_id_sync": "0000000000000000000000000000000000000000000000000000000000000000",
        "trx_id_auth": "0000000000000000000000000000000000000000000000000000000000000000"
        }
    ],
    "more": false
    }
    ```

8. Since Mixin Sync Service can be trusted, therefor, withdraw DO NOT have a oracle process
9. if reqwithdraw abi finished successful, PRS Token API can transfer certain amount of PRS token on Mixin (or Mixin Sync Service can sync "req" table, find all WITHDRAW request with status "1" (REQUESTED) and tranfer PRS token to user account)

10. Mixin Sync Service sync PRS token transfer to prs.tproxy

    ```
    example:
    cleos --url http://178.33.98.152:8888 push action prs.tproxy sync '["123456", true, "{deposit_sync_meta}", "memo"]' -p prs.tproxy@active
    ```

11. prs.tproxy will update and finish request

    ```
    example:

    cleos --url http://178.33.98.152:8888 get table prs.tproxy prs.tproxy req
    {
    "rows": [{
        "req_id": 123456,
        "type": 2,
        "status": 3,
        "user": "testbp2",
        "amount": "1.0000 EOS",
        "sync_content": "{deposit_sync_meta}",
        "auth_content": "",
        "auth_result": 1,
        "time_stamp_req": 1571949304,
        "time_stamp_sync": 1571949704,
        "time_stamp_auth": 0,
        "trx_id_req": "9650e3927e0f433bb05934c98d570a2c984e2d2888ba1e1a3abaddff6f102f22",
        "trx_id_sync": "60c7a7a49110f3ce228ec65000840dbddf95b0697b74110d96eeccee15c8ff13",
        "trx_id_auth": "0000000000000000000000000000000000000000000000000000000000000000"
        }
    ],
    "more": false
    }
    ```
  
12 for all "success" mixin token transfer (from prs to user), related withdraw request will be deleted fro req table in prs.tproxy
## purge table

the following abi can be use to purge data in multi_index_table

- purge "req" table in prs.tproxy    
    `cleos --url http://178.33.98.152:8888 push action prs.tproxy purgeall ["memo"] -p prs.tproxy@active`
- purge "auth" table in proxy.oracle    
    `cleos --url http://178.33.98.152:8888 push action proxy.oracle purgeauth ["memo"] -p proxy.oracle@active`
- pureg "deposit" tablein proxy.oracle    
    `cleos --url http://178.33.98.152:8888 push action proxy.oracle purgedep ["memo"] -p proxy.oracle@active`



    

	










