#!/bin/bash

cleos wallet unlock --password PW5JZzzxW76hMjGgBZmGYbWBcnrF9aGTHmhyPZpP1A2wgR5nBoTgq

#import all keys if not

# eosio system 
private key: 5JbPPXVLYnhPthBinM5eqQhv3TcGygXbtc4bcg1h4sqZmEU6ygi
Public key: EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ

Private key: 5JoSC9AdqxD29uNJxU6SeExB6TjDSj9sDbJANgXeyy7wZGF79i3
Public key: EOS7Lgh7cb3ZdCHpxqhrj3er6ZJiY8aWbKvXvsg4winmsrDp7CDRB

private key: 5Hu2paimuwu9S8aWg9Tap2dWKpjBAJd8eKTei9jKfRcsGsKNmWu
Public key: EOS7Ddt5wpTWfDLuQRKzj6G3emkVNY2jFY7zdKMFVuij23fRRyBjH

#import all keys if not
#prs.prsc
#Private key: 5HrsgbfPEz2K3BnSwb8tVXWYKPn9eRy6XTri3Ew7tvEWAgFn9Yg
#Public key: EOS8f4VNJeXWemFvMtj7kDy4cCAY5cZ6w9z3dUfX8mCdiMJDYJ7es

#prs.oracle
#Private key: 5K4i3ztRuw2ZvTDnHR1xnKqR4xzvHAE4cdsqR4NbAQGEVgNBk1a
#Public key: EOS5sf7hpgCNJKPrLbd8yupV4qf5UAgDWronjgk33HRpDRPHfiLvD

#testuser1
#Private key: 5KDJiuJtgFnyVjL52s8F8GZmHbfWNu5KgSybvpnW48FpvezXVyw
#Public key: EOS69SYW43YvDZxafqXvQszcyAFeY3y5nZTgZfnKVjiLAYwQNdffQ

#testuser2
#Private key: 5KBRqk7hvLW1JwQbJB93SbaqRMCoKVVHodMmmSxSWdZbV79AuXK
#Public key: EOS71hNtcoustBy4zgs4DoKBSJJ6MqHjWvGA7sx8W6VPGjQsTuD9i

#create system accounts
cleos create account eosio eosio.bpay EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos create account eosio eosio.msig EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos create account eosio eosio.names EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos create account eosio eosio.ram EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos create account eosio eosio.ramfee EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos create account eosio eosio.saving EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos create account eosio eosio.stake EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos create account eosio eosio.token EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos create account eosio eosio.vpay EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos create account eosio eosio.rex EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ

# Set eosio.token contract
cleos set contract eosio.token ~/work/eosio.contracts-1.8.x/build/contracts/eosio.token/

# Set eosio.msig contract
cleos set contract eosio.msig ~/work/eosio.contracts-1.8.x/build/contracts/eosio.msig/

# Create and issue SRP token
cleos push action eosio.token create '["eosio", "10000000000.0000 SRP"]' -p eosio.token@active
cleos push action eosio.token issue '["eosio", "10000000000.0000 SRP", "memo"]' -p eosio@active

# Enable PREACTIVATE_FEATURE feature
curl --request POST --url http://127.0.0.1:8888/v1/producer/schedule_protocol_feature_activations -d '{"protocol_features_to_activate": ["0ec7e080177b2c02b278d5088611686b49d739925a92d9bfcacd7fc6b74053bd"]}'

# Set eosio.system contract (v1.8)
cleos set contract eosio ~/work/eosio.contracts-1.8.x/build/contracts/eosio.system/

# Set eosio@active previllage to eosio.msig
cleos push action eosio setpriv '["eosio.msig", 1]' -p eosio@active

# Initial system contract (set default token)
cleos push action eosio init '["0", "4,SRP"]' -p eosio@active

cleos system newaccount --transfer eosio prs.prsc EOS8f4VNJeXWemFvMtj7kDy4cCAY5cZ6w9z3dUfX8mCdiMJDYJ7es --stake-net "1000000.0000 SRP" --stake-cpu "1000000.0000 SRP" --buy-ram "500000.0000 SRP"

cleos system newaccount --transfer eosio prs.oracle EOS5sf7hpgCNJKPrLbd8yupV4qf5UAgDWronjgk33HRpDRPHfiLvD --stake-net "1000000.0000 SRP" --stake-cpu "1000000.0000 SRP" --buy-ram "500000.0000 SRP"

cleos system newaccount --transfer eosio testuser1 EOS69SYW43YvDZxafqXvQszcyAFeY3y5nZTgZfnKVjiLAYwQNdffQ  --stake-net "100.0000 SRP" --stake-cpu "100.0000 SRP" --buy-ram "50.0000 SRP"

cleos system newaccount --transfer eosio testuser2 EOS71hNtcoustBy4zgs4DoKBSJJ6MqHjWvGA7sx8W6VPGjQsTuD9i  --stake-net "10.0000 SRP" --stake-cpu "100.0000 SRP" --buy-ram "50.0000 SRP"

cleos system newaccount --transfer eosio prs.saving EOS62puANYHYHnPvktM8FopjBMbgzkxS3nDAH1rY3G5yc4EZtimqK  --stake-net "10000.0000 SRP" --stake-cpu "10000.0000 SRP" --buy-ram "5000.0000 SRP"

cleos system newaccount --transfer eosio prs.pool EOS5QdbRBZsSGzCaGx1C3A1DoabT5FarN6t3FxBhJjnMRdkMh9mZB --stake-net "10000.0000 SRP" --stake-cpu "10000.0000 SRP" --buy-ram "5000.0000 SRP"
#set contracts
cleos set contract prs.prsc ../smart_contract/build/contracts/prs.prsc/
cleos set contract prs.oracle ../smart_contract/build/contracts/prs.oracle/

cleos push action eosio.token transfer '["eosio", "testuser1", "10.0000 SRP", "memo"]' -p eosio@active
cleos push action eosio.token transfer '["eosio", "testuser2", "10.0000 SRP", "memo"]' -p eosio@active
cleos push action eosio.token transfer '["eosio", "prs.prsc", "100.0000 SRP", "memo"]' -p eosio@active


#set add-code permission (to send action to prs.oracle)
cleos set account permission prs.prsc active --add-code

cleos set account permission testuser1 active  '{"threshold": 1,  "keys":[{"key": "EOS69SYW43YvDZxafqXvQszcyAFeY3y5nZTgZfnKVjiLAYwQNdffQ","weight": 1}], "accounts": [{"permission":{"actor": "prs.prsc", "permission": "eosio.code"},  "weight": 1}]}'  owner -p testuser1@active

cleos set account permission testuser2 active  '{"threshold": 1,  "keys":[{"key": "EOS71hNtcoustBy4zgs4DoKBSJJ6MqHjWvGA7sx8W6VPGjQsTuD9i","weight": 1}], "accounts": [{"permission":{"actor": "prs.prsc", "permission": "eosio.code"},  "weight": 1}]}'  owner -p testuser2@active

