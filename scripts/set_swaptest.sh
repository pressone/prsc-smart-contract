#!/bin/bash

#create system accounts
cleos --url http://51.255.133.170:8888 create account eosio eosio.bpay EOS4zwUF1jf1ZmPGmyqRG1vAt1ytbTbX7meYrE2NsMv1tMK3Nqiv4
cleos --url http://51.255.133.170:8888 create account eosio eosio.msig EOS4zwUF1jf1ZmPGmyqRG1vAt1ytbTbX7meYrE2NsMv1tMK3Nqiv4
cleos --url http://51.255.133.170:8888 create account eosio eosio.name EOS4zwUF1jf1ZmPGmyqRG1vAt1ytbTbX7meYrE2NsMv1tMK3Nqiv4
cleos --url http://51.255.133.170:8888 create account eosio eosio.ram EOS4zwUF1jf1ZmPGmyqRG1vAt1ytbTbX7meYrE2NsMv1tMK3Nqiv4
cleos --url http://51.255.133.170:8888 create account eosio eosio.ramfee EOS4zwUF1jf1ZmPGmyqRG1vAt1ytbTbX7meYrE2NsMv1tMK3Nqiv4
cleos --url http://51.255.133.170:8888 create account eosio eosio.saving EOS4zwUF1jf1ZmPGmyqRG1vAt1ytbTbX7meYrE2NsMv1tMK3Nqiv4
cleos --url http://51.255.133.170:8888 create account eosio eosio.stake EOS4zwUF1jf1ZmPGmyqRG1vAt1ytbTbX7meYrE2NsMv1tMK3Nqiv4
cleos --url http://51.255.133.170:8888 create account eosio eosio.token EOS4zwUF1jf1ZmPGmyqRG1vAt1ytbTbX7meYrE2NsMv1tMK3Nqiv4
cleos --url http://51.255.133.170:8888 create account eosio eosio.vpay EOS4zwUF1jf1ZmPGmyqRG1vAt1ytbTbX7meYrE2NsMv1tMK3Nqiv4
cleos --url http://51.255.133.170:8888 create account eosio eosio.rex EOS4zwUF1jf1ZmPGmyqRG1vAt1ytbTbX7meYrE2NsMv1tMK3Nqiv4

#set all system contracts
cleos  --url http://51.255.133.170:8888 set contract eosio.token ~/work/prs.system.contract/build/contracts/eosio.token/
cleos  --url http://51.255.133.170:8888 set contract eosio.msig ~/work/prs.system.contract/build/contracts/eosio.msig/

#create and issue SRP token
cleos  --url http://51.255.133.170:8888 push action eosio.token create '["eosio", "20000000000.0000 SRP"]' -p eosio.token@active
cleos  --url http://51.255.133.170:8888 push action eosio.token issue '["eosio", "10000000000.0000 SRP", "memo"]' -p eosio@active

#Enable PREACTIVATE_FEATURE feature
curl --request POST --url http://51.255.133.170:8888/v1/producer/schedule_protocol_feature_activations -d '{"protocol_features_to_activate": ["0ec7e080177b2c02b278d5088611686b49d739925a92d9bfcacd7fc6b74053bd"]}'

#Set system contract 
cleos  --url http://51.255.133.170:8888 set contract eosio ~/work/prs.system.contract/build/contracts/eosio.system/

#Set eosio@active previllage to eosio.msig
cleos  --url http://51.255.133.170:8888 push action eosio setpriv '["eosio.msig", 1]' -p eosio@active

#Initial system contract (set default token)
cleos  --url http://51.255.133.170:8888 push action eosio init '["0", "4,SRP"]' -p eosio@active

#Create prs contract accounts
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio prs.prsc  EOS5mEzqVAtUdEgBaHwkAccZ9xSZhu3BucjYmBAKyKxs3Huikkyd5 --stake-net "1000000.0000 SRP" --stake-cpu "1000000.0000 SRP" --buy-ram "500000.0000 SRP"
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio prsc.oracle EOS4xvdmsUHeyc6o3DmpZJZNA1yzBYw4JCQXM3NeVMbahkwiDTr5B --stake-net "1000000.0000 SRP" --stake-cpu "1000000.0000 SRP" --buy-ram "500000.0000 SRP"
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio prs.tproxy EOS7HUaT1bzohHmrMBZpa4YG6PeLFFAwS41gDttCFfYWKjp2UJ13q  --stake-net "1000000.0000 SRP" --stake-cpu "1000000.0000 SRP" --buy-ram "500000.0000 SRP"
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio tprxy.oracle EOS7Go9DuZGQaD6gBPFtR1yoiF9vRZfE4EgmQHG2yAj8jkZa3eBsN  --stake-net "1000000.0000 SRP" --stake-cpu "1000000.0000 SRP" --buy-ram "500000.0000 SRP"
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio prs.ipfs  EOS7NP58mvkGX2Mqx6vaLgXzWuh8QBRkJ4jXoUJ9ZrvrfMZKWkBtX  --stake-net "1000000.0000 SRP" --stake-cpu "1000000.0000 SRP" --buy-ram "500000.0000 SRP"
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio ipfs.oracle  EOS8fwxn8H67nYvRXPiE3vixBbpSvfvJEvnxwZyCDiuKPtDsJYHqK  --stake-net "1000000.0000 SRP" --stake-cpu "1000000.0000 SRP" --buy-ram "500000.0000 SRP"
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio prs.saving EOS62puANYHYHnPvktM8FopjBMbgzkxS3nDAH1rY3G5yc4EZtimqK  --stake-net "1000000.0000 SRP" --stake-cpu "1000000.0000 SRP" --buy-ram "500000.0000 SRP"
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio prs.pool EOS5QdbRBZsSGzCaGx1C3A1DoabT5FarN6t3FxBhJjnMRdkMh9mZB --stake-net "1000000.0000 SRP" --stake-cpu "1000000.0000 SRP" --buy-ram "500000.0000 SRP"
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio pressone5555 EOS7o5nhMZVJMp1fbxo6g3tBhoUFyfcUiGGiA6Jk3fbL4ct5GRfJQ --stake-net "100.0000 SRP" --stake-cpu "100.0000 SRP" --buy-ram "50.0000 SRP"
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio prs.swap EOS5GHctSJA2eBFD6LPiHHWN8gwt4vJFi3FfSP9oQ8w5Y5fUaczqw --stake-net "1000000.0000 SRP" --stake-cpu "1000000.0000 SRP" --buy-ram "500000.0000 SRP"
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio swap.oracle EOS7JA37ZQ9oZ8a8w4hmBzRm4N8FkGN9rzo4hhXLVB6Xnqc8CM6Xn --stake-net "1000000.0000 SRP" --stake-cpu "1000000.0000 SRP" --buy-ram "500000.0000 SRP"
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio swap.pool EOS8bCQHNDUueytqreFRiEQ2W24imLDipJNy1GxHZc2QNZLFHkazZ --stake-net "1000000.0000 SRP" --stake-cpu "1000000.0000 SRP" --buy-ram "500000.0000 SRP"
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio pol.prs.usdt EOS5kARg3K7MiY9oL7BYZituH1dhcpCNgorzAsGmLqzAVfPQ43SgQ --stake-net "1000000.0000 SRP" --stake-cpu "1000000.0000 SRP" --buy-ram "500000.0000 SRP"

#set contracts
cleos  --url http://51.255.133.170:8888 set contract prs.prsc ~/work/prsc-smart-contract/smart_contract/build/contracts/prs.prsc -p prs.prsc@active
cleos  --url http://51.255.133.170:8888 set contract prsc.oracle ~/work/prsc-smart-contract/smart_contract/build/contracts/prsc.oracle/ -p prsc.oracle@active
cleos  --url http://51.255.133.170:8888 set contract prs.tproxy ~/work/prsc-smart-contract/smart_contract/build/contracts/prs.tproxy/ -p prs.tproxy@active
cleos  --url http://51.255.133.170:8888 set contract tprxy.oracle ~/work/prsc-smart-contract/smart_contract/build/contracts/tprxy.oracle/ -p tprxy.oracle@active
cleos  --url http://51.255.133.170:8888 set contract prs.swap ~/work/prsc-smart-contract/smart_contract/build/contracts/prs.swap/ -p prs.swap@active
cleos  --url http://51.255.133.170:8888 set contract swap.oracle ~/work/prsc-smart-contract/smart_contract/build/contracts/swap.oracle/ -p swap.oracle@active

#add code
cleos  --url http://51.255.133.170:8888 set account permission prs.prsc active --add-code
cleos  --url http://51.255.133.170:8888 set account permission prsc.oracle active --add-code
cleos  --url http://51.255.133.170:8888 set account permission prs.tproxy active --add-code
cleos  --url http://51.255.133.170:8888 set account permission tprxy.oracle active --add-code
cleos  --url http://51.255.133.170:8888 set account permission prs.tproxy active --add-code
cleos  --url http://51.255.133.170:8888 set account permission tprxy.oracle active --add-code
cleos  --url http://51.255.133.170:8888 set account permission prs.swap active --add-code
cleos  --url http://51.255.133.170:8888 set account permission swap.oracle active --add-code

cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio testuser1 EOS8XgbSbQsr1wuX5UFDbZTTV76yQWz2BEXV5whTUnHM2w8du2F6S --stake-net "100.0000 SRP" --stake-cpu "100.0000 SRP" --buy-ram "50.0000 SRP"
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio testuser2 EOS58apfmJrAVdyPKBt58mnZ8XtZuFErLiEj7YLQsu9q4bLC1LU63 --stake-net "100.0000 SRP" --stake-cpu "100.0000 SRP" --buy-ram "50.0000 SRP"
cleos  --url http://51.255.133.170:8888 system newaccount --transfer eosio testuser3 EOS6MDSAgD7sHWjtMXpkY3ueyzp3euAsuE15EGG3Epi3kRzdLL7Ah --stake-net "100.0000 SRP" --stake-cpu "100.0000 SRP" --buy-ram "50.0000 SRP"

#create and issue PRSUSDT pool token 
cleos  --url http://51.255.133.170:8888 push action eosio.token create '["eosio", "20000000000.0000 PRSUSDT"]' -p eosio.token@active
cleos  --url http://51.255.133.170:8888 push action eosio.token issue '["eosio", "10000000000.0000 PRSUSDT", "issue PRSUSDT token"]' -p eosio@active

#transfer PRSUSDT token to pol.prs.usdt
cleos  --url http://51.255.133.170:8888 push action eosio.token transfer '["eosio", "pol.prs.usdt", "10000000000.0000 PRSUSDT", "add fund"]' -p eosio@active 

#create and issue USDT token 
cleos  --url http://51.255.133.170:8888 push action eosio.token create '["eosio", "20000000000.0000 USDT"]' -p eosio.token@active
cleos  --url http://51.255.133.170:8888 push action eosio.token issue '["eosio", "10000000000.0000 USDT", "issue USDT token"]' -p eosio@active


