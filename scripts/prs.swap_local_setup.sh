#!/bin/bash

cleos wallet unlock --password PW5JZzzxW76hMjGgBZmGYbWBcnrF9aGTHmhyPZpP1A2wgR5nBoTgq

# import all keys if not
# eosio system 
# private key: 5JbPPXVLYnhPthBinM5eqQhv3TcGygXbtc4bcg1h4sqZmEU6ygi
# Public key: EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ


#create system accounts
cleos -u http://51.255.133.175:8888 create account eosio eosio.bpay EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos -u http://51.255.133.175:8888 create account eosio eosio.msig EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos -u http://51.255.133.175:8888 create account eosio eosio.names EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos -u http://51.255.133.175:8888 create account eosio eosio.ram EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos -u http://51.255.133.175:8888 create account eosio eosio.ramfee EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos -u http://51.255.133.175:8888 create account eosio eosio.saving EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos -u http://51.255.133.175:8888 create account eosio eosio.stake EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos -u http://51.255.133.175:8888 create account eosio eosio.token EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos -u http://51.255.133.175:8888 create account eosio eosio.vpay EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ
cleos -u http://51.255.133.175:8888 create account eosio eosio.rex EOS6ZpXhzU4Kuz66cHpQEUeGYDhu8aD5cWxDgB6QXwg224aQrX2ZZ

# Set eosio.token contract
cleos -u http://51.255.133.175:8888 set contract eosio.token ~/work/prs.system.contract/build/contracts/eosio.token/

# Set eosio.msig contract
cleos -u http://51.255.133.175:8888 set contract eosio.msig ~/work/prs.system.contract/build/contracts/eosio.msig/

# Create and issue SRP token
cleos -u http://51.255.133.175:8888 push action eosio.token create '["eosio", "10000000000.0000 EOS"]' -p eosio.token@active
cleos -u http://51.255.133.175:8888 push action eosio.token issue '["eosio", "10000000000.0000 EOS", "memo"]' -p eosio@active

# Enable PREACTIVATE_FEATURE feature
curl --request POST --url http://51.255.133.175:8888/v1/producer/schedule_protocol_feature_activations -d '{"protocol_features_to_activate": ["0ec7e080177b2c02b278d5088611686b49d739925a92d9bfcacd7fc6b74053bd"]}'

# Set eosio.system contract (v1.8)
cleos -u http://51.255.133.175:8888 set contract eosio ~/work/prs.system.contract/build/contracts/eosio.system/

# Set eosio@active previllage to eosio.msig
cleos -u http://51.255.133.175:8888 push action eosio setpriv '["eosio.msig", 1]' -p eosio@active

# Initial system contract (set default token)
cleos -u http://51.255.133.175:8888 push action eosio init '["0", "4,EOS"]' -p eosio@active

cleos  --url http://51.255.133.175:8888 system newaccount --transfer eosio evmcontract EOS51Ub7oKFUn7uuCWwtQUtqmDNSasCE3QsZxHuZW1sq7x7mLacEY --stake-net "1000000.0000 EOS" --stake-cpu "1000000.0000 EOS" --buy-ram "500000.0000 EOS"

cleos  --url http://51.255.133.175:8888 system newaccount --transfer eosio erc2basic EOS6XNETF2ChL9Z6JQAQwGt1J7T59nBWG4cdePjWHpqAagToGNcge --stake-net "1000000.0000 EOS" --stake-cpu "1000000.0000 EOS" --buy-ram "500000.0000 EOS"

cleos  --url http://51.255.133.175:8888 transfer eosio erc2basic "1000000.0000 EOS" "deposit" -p eosio@active

cleos --url http://51.255.133.175:8888 set contract evmcontract ~/work/eos_evm/eosio.evm/eosio.evm/

cleos -u http://51.255.133.175:8888 set account permission evmcontract active --add-code

cleos  --url http://51.255.133.175:8888 system newaccount --transfer eosio receiver EOS7mWMxo2MeQtwhmN87p9xafoCwWF9bqqEZZa4BkTJ45VwEsrMiM --stake-net "1000000.0000 EOS" --stake-cpu "1000000.0000 EOS" --buy-ram "500000.0000 EOS"
